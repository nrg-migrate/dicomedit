/**
 * Copyright (c) 2009,2011 Washington University
 */
package org.nrg.io;

import java.io.File;

import junit.framework.TestCase;

import com.google.common.base.Function;

/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public class PresuffixFileMapperTest extends TestCase {

    /**
     * Test method for {@link org.nrg.io.PresuffixFileMapper#apply(java.io.File)}.
     */
    public void testMap() {
        final Function<File,File> m = new PresuffixFileMapper("-mod");
        final File f1 = m.apply(new File("foo"));
        assertEquals("foo-mod", f1.getName());
        final File f2 = m.apply(new File("bar/baz.dat"));
        assertEquals("bar" + File.separator + "baz-mod.dat", f2.getPath());
    }
}
