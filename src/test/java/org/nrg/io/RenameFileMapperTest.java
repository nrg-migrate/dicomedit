/**
 * Copyright (c) 2009 Washington University
 */
package org.nrg.io;

import java.io.File;

import org.nrg.io.RenameFileMapper;

import junit.framework.TestCase;

/**
 * @author Kevin A. Archie <karchie@npg.wustl.edu>
 *
 */
public class RenameFileMapperTest extends TestCase {

    /**
     * Test method for {@link org.nrg.io.RenameFileMapper#apply(java.io.File)}.
     */
    public void testMap() {
	final File f1 = new RenameFileMapper("{0}").apply(new File("foo/bar"));
	assertEquals("foo" + File.separator + "bar", f1.getPath());
	
	final File f2 = new RenameFileMapper("a{0}b").apply(new File("baz/yak.bar"));
	assertEquals("baz" + File.separator + "ayak.barb", f2.getPath());
    }
}
