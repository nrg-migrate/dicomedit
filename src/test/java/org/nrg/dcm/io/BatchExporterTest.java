/**
 * Copyright (c) 2011 Washington University
 */
package org.nrg.dcm.io;

import java.io.File;
import java.net.URISyntaxException;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import com.google.common.io.Resources;
import junit.framework.TestCase;

import org.easymock.MockControl;
import org.nrg.dcm.edit.Action;
import org.nrg.dcm.edit.AttributeMissingException;
import org.nrg.dcm.edit.Operation;
import org.nrg.dcm.edit.Statement;
import org.nrg.io.FileWalkIterator;
import org.nrg.util.EditProgressMonitor;

import com.google.common.collect.Lists;

/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public class BatchExporterTest extends TestCase {
    private static final File sampleDataDir;
    static {
        try {
             sampleDataDir = new File(Resources.getResource("dicom/1.MR.head_DHead.4.1.20061214.091206.156000.1632817982.dcm.gz").toURI()).getParentFile();
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }
    private static final int nExports = sampleDataDir.list().length;
    
    private MockControl exporterControl,
    exportsIteratorControl,
    progressMonitorControl,
    actionControl,
    operationControl;
    private Iterator<File> sampleDataIterator;

    protected void setUp() {
        exporterControl = MockControl.createControl(DicomObjectExporter.class);
        exportsIteratorControl = MockControl.createControl(Iterator.class);
        progressMonitorControl = MockControl.createControl(EditProgressMonitor.class);
        actionControl = MockControl.createControl(Action.class);
        operationControl = MockControl.createControl(Operation.class);
        
        sampleDataIterator = new FileWalkIterator(sampleDataDir, null);
    }

    /**
     * Test method for {@link org.nrg.dcm.io.BatchExporter#BatchExporter(DicomObjectExporter, List, Iterable)}.
     */
    public void testBatchExporterDicomObjectExporterStatementListIterator() {
        final DicomObjectExporter exporter = (DicomObjectExporter) exporterControl.getMock();
        final List<Statement> statements = Lists.newArrayList();
        @SuppressWarnings("unchecked")
	final Iterator<File> exports = (Iterator<File>) exportsIteratorControl.getMock();
        final BatchExporter be = new BatchExporter(exporter, statements, exports);
        assertNotNull(be);
    }

    /**
     * Test method for {@link org.nrg.dcm.io.BatchExporter#BatchExporter(DicomObjectExporter, List, Iterable)}.
     */
    public void testBatchExporterDicomObjectExporterStatementListCollection() {
        final DicomObjectExporter exporter = (DicomObjectExporter) exporterControl.getMock();
        final List<Statement> statements = Lists.newArrayList();
        @SuppressWarnings("unchecked")
	final Iterator<File> exports = (Iterator<File>) exportsIteratorControl.getMock();
        final BatchExporter be = new BatchExporter(exporter, statements, exports);
        assertNotNull(be);
    }

    /**
     * Test method for {@link org.nrg.dcm.io.BatchExporter#setProgressMonitor(org.nrg.util.EditProgressMonitor, int)}.
     */
    public void testSetProgressMonitor() {
        final DicomObjectExporter exporter = (DicomObjectExporter) exporterControl.getMock();
        final List<Statement> statements = Lists.newArrayList();
        @SuppressWarnings("unchecked")
	final Iterator<File> exports = (Iterator<File>) exportsIteratorControl.getMock();
        final BatchExporter be = new BatchExporter(exporter, statements, exports);
        final EditProgressMonitor pm = (EditProgressMonitor) progressMonitorControl.getMock();
        be.setProgressMonitor(pm, 0);
        assertTrue(0 == be.getProgress());
        be.setProgressMonitor(pm, 42);
        assertTrue(42 == be.getProgress());
    }

    /**
     * Test method for {@link org.nrg.dcm.io.BatchExporter#isPending()}.
     */
    public void testIsPending() {
        final DicomObjectExporter exporter = (DicomObjectExporter) exporterControl.getMock();
        final List<Statement> statements = Lists.newArrayList();
        @SuppressWarnings("unchecked")
	final Iterator<File> exports = (Iterator<File>) exportsIteratorControl.getMock();
        final BatchExporter be = new BatchExporter(exporter, statements, exports);
        assertTrue(be.isPending());
        be.run();
        assertFalse(be.isPending());
    }

    /**
     * Test method for {@link org.nrg.dcm.io.BatchExporter#getFailures()}.
     */
    public void testGetFailures() {
        final DicomObjectExporter exporter = (DicomObjectExporter) exporterControl.getMock();
        final List<Statement> statements = Lists.newArrayList();
        @SuppressWarnings("unchecked")
	final Iterator<File> exports = (Iterator<File>) exportsIteratorControl.getMock();
        final BatchExporter be = new BatchExporter(exporter, statements, exports);
        try {
            be.getFailures();
            fail("missed expected IllegalStateException");
        } catch (IllegalStateException ignored) {}
        be.run();
        assertTrue(be.getFailures().isEmpty());
    }

    /**
     * Test method for {@link org.nrg.dcm.io.BatchExporter#getProgress()}.
     */
    public void testGetProgress() {
        final DicomObjectExporter exporter = (DicomObjectExporter) exporterControl.getMock();
        final List<Statement> statements = Lists.newArrayList();
        @SuppressWarnings("unchecked")
	final Iterator<File> exports = (Iterator<File>) exportsIteratorControl.getMock();
        final BatchExporter be = new BatchExporter(exporter, statements, exports);
        final EditProgressMonitor pm = (EditProgressMonitor) progressMonitorControl.getMock();
        be.setProgressMonitor(pm, 0);
        assertTrue(0 == be.getProgress());
        be.setProgressMonitor(pm, 42);
        assertTrue(42 == be.getProgress());
    }

    /**
     * Test method for {@link org.nrg.dcm.io.BatchExporter#run()}.
     */
    public void testRun() throws Exception {
        final DicomObjectExporter exporter = (DicomObjectExporter) exporterControl.getMock();
        final List<Statement> statements = Collections.emptyList();
//        statements.getActions(null, null);
//        statementsControl.setMatcher(MockControl.ALWAYS_MATCHER);
//        statementsControl.setReturnValue(Collections.emptyList(), nExports);
//        statementsControl.replay();
        
        final BatchExporter be = new BatchExporter(exporter, statements, sampleDataIterator);
        be.run();
        assertTrue(be.getFailures().isEmpty());
    }
    
    /**
     * Test method for {@link org.nrg.dcm.io.BatchExporter#run()}.
     */
    public void testRunWithExportErrors() throws Exception {
        final DicomObjectExporter exporter = (DicomObjectExporter) exporterControl.getMock();
        exporter.export(null, null);
        exporterControl.setMatcher(MockControl.ALWAYS_MATCHER);
        exporterControl.setThrowable(new OutOfMemoryError(), nExports);
        exporter.close();
        exporterControl.setVoidCallable();
        exporterControl.replay();
        
        final List<Statement> statements = Lists.newArrayList();
//        final StatementList statements = (StatementList) statementsControl.getMock();
//        statements.getActions(null, null);
//        statementsControl.setMatcher(MockControl.ALWAYS_MATCHER);
//        statementsControl.setReturnValue(Collections.emptyList(), nExports);
//        statementsControl.replay();

        final BatchExporter be = new BatchExporter(exporter, statements, sampleDataIterator);
        be.run();
        assertTrue(nExports == be.getFailures().size());
    }
    
    /**
     * Test method for {@link org.nrg.dcm.io.BatchExporter#run()}.
     */
    public void testRunWithScriptErrors() throws Exception {
        final int nWorking = 5;
        
        final DicomObjectExporter exporter = (DicomObjectExporter) exporterControl.getMock();
        exporter.export(null, null);
        exporterControl.setMatcher(MockControl.ALWAYS_MATCHER);
        exporterControl.setVoidCallable(nExports);
        exporter.close();
        exporterControl.setVoidCallable();
        exporterControl.replay();
        
        final Action action = (Action) actionControl.getMock();
        action.apply();
        actionControl.setVoidCallable(nWorking);
        actionControl.replay();

        final Operation operation = (Operation) operationControl.getMock();
        operation.makeAction(null);
        operationControl.setMatcher(MockControl.ALWAYS_MATCHER);
        operationControl.setReturnValue(action, nWorking);
        operationControl.setDefaultThrowable(new AttributeMissingException(0));
        operationControl.replay();
        
        final Statement statement = new Statement(null, operation);
        final List<Statement> statements = Lists.newArrayList(statement);        
        final BatchExporter be = new BatchExporter(exporter, statements, sampleDataIterator);
        be.run();
        assertEquals(nExports - nWorking, be.getFailures().size());
    }
}
