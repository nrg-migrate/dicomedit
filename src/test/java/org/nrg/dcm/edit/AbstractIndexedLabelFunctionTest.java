package org.nrg.dcm.edit;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.dcm4che2.data.DicomObject;

import com.google.common.collect.Lists;

import junit.framework.TestCase;

/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public class AbstractIndexedLabelFunctionTest extends TestCase {
    final class TestLabelFunction extends AbstractIndexedLabelFunction {
        private final Set<String> defined;
        
        public TestLabelFunction(final String...defined) {
            this.defined = new HashSet<String>(Arrays.asList(defined));
        }
        
        public boolean isAvailable(final String label) {
            return !defined.contains(label);
        }
    }
    
    private List<Value> format(final String format) {
        final List<Value> l = Lists.newArrayList();
        l.add(new ConstantValue(format));
        return l;
    }
    
    private final DicomObject o = null;
    
    public void testApply() throws ScriptEvaluationException{
        assertEquals("", new TestLabelFunction().apply(format("")).on(o));
        assertEquals("0", new TestLabelFunction().apply(format("#")).on(o));
        assertEquals("1", new TestLabelFunction("0").apply(format("#")).on(o));
        assertEquals("00", new TestLabelFunction().apply(format("##")).on(o));
        assertEquals("01", new TestLabelFunction("00").apply(format("##")).on(o));
        assertEquals("foo002bar", new TestLabelFunction("foo000bar","foo001bar")
            .apply(format("foo###bar")).on(o));
    }
}
