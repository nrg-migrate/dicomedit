/**
 * Copyright (c) 2011 Washington University
 */
package org.nrg.dcm.edit;

import java.util.Calendar;

import org.dcm4che2.data.BasicDicomObject;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.Tag;
import org.dcm4che2.data.UID;
import org.dcm4che2.data.VR;

import junit.framework.TestCase;

/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public class DicomUtilsTest extends TestCase {
	/**
	 * Test method for {@link org.nrg.dcm.edit.DicomUtils#getString(org.dcm4che2.data.DicomObject, int)}.
	 */
	public void testGetString() throws Exception {
		final DicomObject d = new BasicDicomObject();
		assertNull(DicomUtils.getString(d, Tag.ImageType));
		
		d.putStrings(Tag.ImageType, VR.CS, new String[]{"A", "B", "C"});
		assertEquals("A\\B\\C", DicomUtils.getString(d, Tag.ImageType));
		
		d.putSequence(Tag.InstitutionCodeSequence);
		try {
			DicomUtils.getString(d, Tag.InstitutionCodeSequence);
			fail("Missed expected AttributeVRMismatchException for element type SQ");
		} catch (AttributeVRMismatchException ok) {}
		
		d.putBytes(0x00170001, VR.UN, "foo".getBytes());
		assertEquals("foo", DicomUtils.getString(d, 0x00170001));
		
		d.putInts(0x00170002, VR.AT, new int[]{0x01, 0x10, 0x1f, 0xff});
		assertEquals("1\\16\\31\\255", DicomUtils.getString(d, 0x00170002));
		
		d.putBytes(0x00170003, VR.OB, new byte[]{(byte)0x0d, (byte)0xe1, (byte)0xfe, (byte)0x1f});
		assertEquals("0D\\E1\\FE\\1F", DicomUtils.getString(d, 0x00170003));
	}

	/**
	 * Test method for {@link org.nrg.dcm.edit.DicomUtils#isValidUID(java.lang.CharSequence)}.
	 */
	public void testIsValidUID() {
		assertFalse(DicomUtils.isValidUID(null));
		assertFalse(DicomUtils.isValidUID(""));
		assertFalse(DicomUtils.isValidUID("a"));
		assertTrue(DicomUtils.isValidUID("0"));
		assertTrue(DicomUtils.isValidUID("1"));
		assertFalse(DicomUtils.isValidUID("01"));
		assertTrue(DicomUtils.isValidUID("10"));
		assertTrue(DicomUtils.isValidUID("0.1"));
		assertTrue(DicomUtils.isValidUID("0.12"));
		assertFalse(DicomUtils.isValidUID("0.x2"));
		assertTrue(DicomUtils.isValidUID("0.1234567890"));
		assertTrue(DicomUtils.isValidUID("0.1.2.3.4.5.6.7.8.9.10"));
		assertTrue(DicomUtils.isValidUID("0.1234567890.1234567890.1234567890.1234567890.1234567890.1234567"));
		assertFalse(DicomUtils.isValidUID("0.1234567890.1234567890.1234567890.1234567890.1234567890.12345678")); // too long
	}

	/**
	 * Test method for {@link org.nrg.dcm.edit.DicomUtils#getTransferSyntaxUID(org.dcm4che2.data.DicomObject)}.
	 */
	public void testGetTransferSyntaxUID() {
		final DicomObject o1 = new BasicDicomObject();
		o1.putString(Tag.TransferSyntaxUID, VR.UI, UID.ExplicitVRBigEndian);
		assertEquals(UID.ExplicitVRBigEndian, DicomUtils.getTransferSyntaxUID(o1));
		
		final DicomObject o2 = new BasicDicomObject();
		assertEquals(UID.ImplicitVRLittleEndian, DicomUtils.getTransferSyntaxUID(o2));
	}

	/*
	 * TODO: tests for all the read methods
	 */
	
	/**
	 * Test method for {@link org.nrg.dcm.edit.DicomUtils#getDateTime(org.dcm4che2.data.DicomObject, int, int)}.
	 */
	public void testGetDateTime() {
		final Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.MILLISECOND, 0);

		final DicomObject o = new BasicDicomObject();
		
		o.putString(Tag.StudyDate, VR.DA, "20110121");
		calendar.set(2011, 0, 21, 0, 0, 0);
		assertEquals(calendar.getTime(), DicomUtils.getDateTime(o, Tag.StudyDate, Tag.StudyTime));
		
		o.putString(Tag.StudyTime, VR.TM, "151415");
		calendar.set(2011, 0, 21, 15, 14, 15);
		assertEquals(calendar.getTime(), DicomUtils.getDateTime(o, Tag.StudyDate, Tag.StudyTime));
		
		calendar.set(Calendar.MILLISECOND, 927);
		o.putString(Tag.StudyTime, VR.TM, "151415.927");
		assertEquals(calendar.getTime(), DicomUtils.getDateTime(o, Tag.StudyDate, Tag.StudyTime));
		
		o.remove(Tag.StudyDate);
		calendar.set(1970, 0, 1);
		assertEquals(calendar.getTime(), DicomUtils.getDateTime(o, Tag.StudyDate, Tag.StudyTime));
	}
}
