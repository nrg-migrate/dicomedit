/**
 * Copyright (c) 2012 Washington University
 */
package org.nrg.dcm.edit;

import java.util.Collections;

import org.dcm4che2.data.BasicDicomObject;
import org.dcm4che2.data.Tag;

import junit.framework.TestCase;


/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public class NoOpTest extends TestCase {

    /**
     * Test method for {@link org.nrg.dcm.edit.NoOp#makeAction(org.dcm4che2.data.DicomObject)}.
     */
    public void testMakeAction() throws AttributeException,ScriptEvaluationException {
        final Operation noop = new NoOp();
        final Action noa = noop.makeAction(new BasicDicomObject());
        noa.apply();    // TODO: make this a mock and verify zero interactions
    }

    /**
     * Test method for {@link org.nrg.dcm.edit.NoOp#apply(java.util.Map)}.
     */
    public void testApply() throws ScriptEvaluationException {
        final Operation noop = new NoOp();
        assertEquals(null, noop.apply(Collections.singletonMap(Tag.StudyInstanceUID, "1.2.3.4")));
    }
}
