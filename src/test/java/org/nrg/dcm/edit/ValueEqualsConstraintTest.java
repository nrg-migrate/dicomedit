/**
 * Copyright (c) 2012 Washington University
 */
package org.nrg.dcm.edit;

import org.dcm4che2.data.BasicDicomObject;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.Tag;
import org.dcm4che2.data.VR;

import junit.framework.TestCase;

/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public class ValueEqualsConstraintTest extends TestCase {
    /**
     * Test method for {@link org.nrg.dcm.edit.AbstractValueConstraint#matches(org.dcm4che2.data.DicomObject)}.
     */
    public void testMatchesDicomObject() throws ScriptEvaluationException {
        final DicomObject o1 = new BasicDicomObject();
        o1.putString(Tag.StudyDescription, VR.LO, "foo");
        
        final DicomObject o2 = new BasicDicomObject();
        o2.putString(Tag.StudyDescription, VR.LO, "bar");
        
        final Value studyDesc = new SingleTagValue(Tag.StudyDescription);
        final ConstraintMatch m1 = new ValueEqualsConstraint(studyDesc, new ConstantValue("foo"));
        assertTrue(m1.matches(o1));
        assertFalse(m1.matches(o2));
        
        final ConstraintMatch m2 = new ValueEqualsConstraint(new ConstantValue("bar"), studyDesc);
        assertFalse(m2.matches(o1));
        assertTrue(m2.matches(o2));
        
        final Value patientName = new SingleTagValue(Tag.PatientName);
        final Value nullVal = new ConstantValue(null);
        
        final ConstraintMatch m3 = new ValueEqualsConstraint(studyDesc, nullVal);
        assertFalse(m3.matches(o1));
        final ConstraintMatch m4 = new ValueEqualsConstraint(patientName, nullVal);
        assertTrue(m4.matches(o1));
        
        final ConstraintMatch m5 = new ValueEqualsConstraint(patientName, new ConstantValue(""));
        assertFalse(m5.matches(o1));
    }
}
