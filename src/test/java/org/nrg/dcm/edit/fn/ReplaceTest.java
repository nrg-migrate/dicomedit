/**
 * Copyright (c) 2010,2011 Washington University
 */
package org.nrg.dcm.edit.fn;

import java.io.File;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.Collections;
import java.util.Map;

import com.google.common.io.Resources;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.Tag;
import org.nrg.dcm.DicomUtils;
import org.nrg.dcm.edit.ConstantValue;
import org.nrg.dcm.edit.ScriptEvaluationException;
import org.nrg.dcm.edit.ScriptFunction;
import org.nrg.dcm.edit.SingleTagValue;
import org.nrg.dcm.edit.Value;

import junit.framework.TestCase;

/**
 * 
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public class ReplaceTest extends TestCase {
    private static final File f4;

    static {
        try {
            f4 = new File(Resources.getResource("dicom/1.MR.head_DHead.4.1.20061214.091206.156000.1632817982.dcm.gz").toURI());
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }

    private static final Map<Integer,String> context = Collections.emptyMap();

    private static final String orig_str = "fooxbarxbazxfooxfrob";
    private static final Value original = new ConstantValue(orig_str);

    public void testApply() throws Exception {
        final ScriptFunction replace = new Replace();
        assertEquals("_xbarxbazx_xfrob", replace.apply(Arrays.asList(new Value[] {
                original, new ConstantValue("foo"), new ConstantValue("_")
        })).on(context));
        assertEquals(orig_str, replace.apply(Arrays.asList(new Value[] {
                original, new ConstantValue("none"), new ConstantValue("_")
        })).on(context));

        final DicomObject o4 = DicomUtils.read(f4);
        assertEquals("Sample*ID", replace.apply(Arrays.asList(new Value[] {
                new SingleTagValue(Tag.PatientID), new ConstantValue(" "), new ConstantValue("*")
        })).on(o4));
        assertEquals("Sample ID", replace.apply(Arrays.asList(new Value[] {
                new SingleTagValue(Tag.PatientID), new ConstantValue("*"), new ConstantValue("$")
        })).on(o4));
        assertEquals("MEDPC ID", replace.apply(Arrays.asList(new Value[] {
                new SingleTagValue(Tag.PatientID), new ConstantValue("Sample"), new SingleTagValue(Tag.StationName)
        })).on(o4));

        try {
            replace.apply(Arrays.asList(new Value[] {
                    original, new ConstantValue("x")
            }));
            fail("Expected ScriptEvaluationException for replace[original, pre]");
        } catch (ScriptEvaluationException ok) {}

        try {
            replace.apply(Arrays.asList(new Value[] {
                    original
            }));
            fail("Expected ScriptEvaluationException for replace[original]");
        } catch (ScriptEvaluationException ok) {}

        try {
            replace.apply(Arrays.asList(new Value[] {}));
            fail("Expected ScriptEvaluationException for replace[]");
        } catch (ScriptEvaluationException ok) {}
    }
}
