package org.nrg.dcm.edit.fn;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.dcm4che2.data.DicomObject;
import org.nrg.dcm.edit.ConstantValue;
import org.nrg.dcm.edit.ScriptEvaluationException;
import org.nrg.dcm.edit.Value;

import junit.framework.TestCase;

public class UrlEncodeTest extends TestCase {
    private static final DicomObject nobj = null;
    private static final Value v0 = new ConstantValue("no-change-needed");
    private static final List<Value> v0l = Collections.singletonList(v0);
    private static final Value v1 = new ConstantValue("has space");
    private static final List<Value> v1l = Collections.singletonList(v1);
    private static final Value v2 = new ConstantValue("lots of $%^@! characters");
    private static final List<Value> v2l = Collections.singletonList(v2);
    
    public void testApplyNoArgs() {
        final UrlEncode u = new UrlEncode();
        try {
            u.apply(new ArrayList<Value>());
            fail("expected ScriptEvaluationException for empty arguments list");
        } catch (ScriptEvaluationException ok) {}
    }
    
    public void testApply() throws ScriptEvaluationException,UnsupportedEncodingException {
        final UrlEncode u = new UrlEncode();
        assertEquals("no-change-needed", u.apply(v0l).on(nobj));
        assertEquals("has+space", u.apply(v1l).on(nobj));
        assertEquals("lots+of+%24%25%5E%40%21+characters", u.apply(v2l).on(nobj));
        assertEquals(v2.on(nobj), URLDecoder.decode(u.apply(v2l).on(nobj), "UTF-8"));
    }
}
