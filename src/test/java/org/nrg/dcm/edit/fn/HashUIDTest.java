/**
 * Copyright (c) 2012 Washington University
 */
package org.nrg.dcm.edit.fn;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import com.google.common.io.Resources;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.Tag;
import org.dcm4che2.data.UID;
import org.dcm4che2.util.UIDUtils;
import org.nrg.dcm.DicomUtils;
import org.nrg.dcm.edit.ConstantValue;
import org.nrg.dcm.edit.ScriptEvaluationException;
import org.nrg.dcm.edit.SingleTagValue;
import org.nrg.dcm.edit.Value;

import com.google.common.base.Function;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import junit.framework.TestCase;

/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public class HashUIDTest extends TestCase {
    private static final File f4;

    static {
        try {
            f4 = new File(Resources.getResource("dicom/1.MR.head_DHead.4.1.20061214.091206.156000.1632817982.dcm.gz").toURI());
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }

    public void testApplyValidity() throws ScriptEvaluationException {
        final Value v = new ConstantValue("foo");
        final Value uidv = new HashUID().apply(Arrays.asList(v));
        final String uid = uidv.on(Collections.<Integer,String>emptyMap());
        assertTrue(UIDUtils.isValidUID(uid));
        assertTrue(uid.startsWith("2.25."));
    }

    public void testApplyReproducibility() throws ScriptEvaluationException {
        final Value v1 = new ConstantValue("bar");
        final Value v2 = new ConstantValue("bar");
        final Value uidv1 = new HashUID().apply(Arrays.asList(v1));
        final Value uidv2 = new HashUID().apply(Arrays.asList(v2));
        final String uid1 = uidv1.on(Collections.<Integer,String>emptyMap());
        final String uid2 = uidv2.on(Collections.<Integer,String>emptyMap());
        assertEquals(uid1, uid2);
    }

    public void testApplyUniqueness() throws ScriptEvaluationException {
        final List<? extends Value> vs = Lists.newArrayList(new ConstantValue(UID.MRImageStorage),
                new ConstantValue(UID.CTImageStorage),
                new ConstantValue(UID.ExplicitVRBigEndian),
                new ConstantValue(UID.JPEG2000));
        final List<String> uids = Lists.transform(vs, new Function<Value,String>() {
            public String apply(final Value v) {
                try {
                    return new HashUID().apply(Arrays.asList(v)).on(Collections.<Integer,String>emptyMap());
                } catch (ScriptEvaluationException e) {
                    throw new RuntimeException(e);
                }
            }
        });
        assertEquals(vs.size(), uids.size());
        assertEquals(uids.size(), Sets.newLinkedHashSet(uids).size());
    }
    
    public void testApplyToEmptyString() throws ScriptEvaluationException {
        final Value v = new ConstantValue("");
        final Value uidv = new HashUID().apply(Arrays.asList(v));
        final String uid = uidv.on(Collections.<Integer,String>emptyMap());
        assertTrue(UIDUtils.isValidUID(uid));
        assertTrue(uid.startsWith("2.25."));
    }
    
    public void testApplyToDicomObject() throws IOException,ScriptEvaluationException {
        final DicomObject o = DicomUtils.read(f4);
        final Value v1 = new SingleTagValue(Tag.StudyInstanceUID);
        final Value v2 = new SingleTagValue(Tag.SeriesInstanceUID);
        final Value v3 = new SingleTagValue(Tag.SOPInstanceUID);
        final Value uidv1 = new HashUID().apply(Arrays.asList(v1));
        final Value uidv2 = new HashUID().apply(Arrays.asList(v2));
        final Value uidv3 = new HashUID().apply(Arrays.asList(v3));
        final String uid1 = uidv1.on(o);
        final String uid2 = uidv2.on(o);
        final String uid3 = uidv3.on(o);
        assertTrue(UIDUtils.isValidUID(uid1));
        assertTrue(UIDUtils.isValidUID(uid2));
        assertTrue(UIDUtils.isValidUID(uid3));
        assertFalse(uid1.equals(uid2));
        assertFalse(uid2.equals(uid3));
        assertFalse(uid3.equals(uid1));
    }
    
    public void testApplyToNullValue() throws ScriptEvaluationException {
        final Value v = new ConstantValue(null);
        final Value uidv = new HashUID().apply(Arrays.asList(v));
        assertNotNull(uidv);
        final String uid = uidv.on(Collections.<Integer,String>emptyMap());
        assertNull(uid);
    }
}
