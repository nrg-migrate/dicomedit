/**
 * Copyright (c) 2011 Washington University
 */
package org.nrg.dcm.edit.fn;

import java.util.ArrayList;
import java.util.Collections;

import org.dcm4che2.data.DicomObject;
import org.nrg.dcm.edit.ConstantValue;
import org.nrg.dcm.edit.ScriptEvaluationException;
import org.nrg.dcm.edit.Value;

import junit.framework.TestCase;

/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public class UppercaseTest extends TestCase {
    private static final DicomObject nobj = null;
    private static final Value vfoo = new ConstantValue("foo");
    private static final Value vBar = new ConstantValue("Bar");
    private static final Value v1bAzbang = new ConstantValue("1bAz!");

    /**
     * Test method for {@link org.nrg.dcm.edit.fn.Uppercase#apply(java.util.List)}.
     */
    public void testApplyNoArgs() {
        final Uppercase u = new Uppercase();
        try {
            u.apply(new ArrayList<Value>());
            fail("expected ScriptEvaluationException for empty arguments list");
        } catch (ScriptEvaluationException ok) {}
    }

    /**
     * Test method for {@link org.nrg.dcm.edit.fn.Uppercase#apply(java.util.List)}.
     */
    public void testApply() throws ScriptEvaluationException {
        final Uppercase u = new Uppercase();
        assertEquals("FOO", u.apply(Collections.singletonList(vfoo)).on(nobj));
        assertEquals("BAR", u.apply(Collections.singletonList(vBar)).on(nobj));
        assertEquals("1BAZ!", u.apply(Collections.singletonList(v1bAzbang)).on(nobj));
    }
}
