package org.nrg.dcm.edit.fn;

import java.io.IOException;
import java.net.URL;
import java.util.Arrays;
import java.util.Map;

import junit.framework.TestCase;

import org.nrg.dcm.edit.ConstantValue;
import org.nrg.dcm.edit.Value;
import org.nrg.util.IHttpClient;

import com.google.common.collect.Maps;

public class GetURLTest extends TestCase {
	private FakeGetURL function;
	private Map<Integer,String> context;

	protected void setUp() throws Exception {
		function = new FakeGetURL();
		context = Maps.newHashMap();
	}

	public void testApply() throws Exception {
		function.response = "foobar";
		Value value = function.apply(Arrays.asList(
				new Value[] { new ConstantValue("http://nrg.wustl.edu/something") }
		));
		assertEquals("foobar", value.on(context));
	}

	public void testApplyExtraWhitespace() throws Exception {
		function.response = "foobar \n\t";
		Value value = function.apply(Arrays.asList(
				new Value[] { new ConstantValue("http://nrg.wustl.edu/something") }
		));
		assertEquals("foobar", value.on(context));
	}

	private static class FakeGetURL extends GetURL {
		String response;

		protected IHttpClient getHttpClient() {
			return new IHttpClient() {
				public String request(URL url) throws IOException {
					return response;
				}
			};
		}
	}
}
