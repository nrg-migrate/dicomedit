/**
 * Copyright (c) 2010,2011 Washington University
 */
package org.nrg.dcm.edit.fn;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.Collections;
import java.util.Map;

import com.google.common.io.Resources;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.Tag;
import org.nrg.dcm.DicomUtils;
import org.nrg.dcm.edit.ConstantValue;
import org.nrg.dcm.edit.IntegerValue;
import org.nrg.dcm.edit.ScriptEvaluationException;
import org.nrg.dcm.edit.ScriptFunction;
import org.nrg.dcm.edit.SingleTagValue;
import org.nrg.dcm.edit.Value;

import junit.framework.TestCase;

/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public class MatchTest extends TestCase {
    private static final File f4;

    static {
        try {
            f4 = new File(Resources.getResource("dicom/1.MR.head_DHead.4.1.20061214.091206.156000.1632817982.dcm.gz").toURI());
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }

    private static final Map<Integer,String> context = Collections.emptyMap();

    /**
     * Test method for {@link org.nrg.dcm.edit.fn.Match#apply(java.util.List)}.
     * @throws IOException 
     * @throws DicomObjectException 
     */
    public void testApply() throws ScriptEvaluationException,IOException {
        final ScriptFunction match = new Match();
        assertEquals("v00", match.apply(Arrays.asList(new Value[] {
                new ConstantValue("s01_v00_mr1"),
                new ConstantValue(".*_(v[0-9]+)_.*"),
                new IntegerValue("1")
        })).on(context));

        assertEquals("s01_v00_mr1", match.apply(Arrays.asList(new Value[] {
                new ConstantValue("s01_v00_mr1"),
                new ConstantValue(".*_(v[0-9]+)_.*"),
                new IntegerValue("0")
        })).on(context));

        assertNull(match.apply(Arrays.asList(new Value[] {
                new ConstantValue("s01+v00+mr1"),
                new ConstantValue(".*_(v[0-9]+)_.*"),
                new IntegerValue("1")
        })).on(context));

        final DicomObject o4 = DicomUtils.read(f4);
        assertEquals("ID", match.apply(Arrays.asList(new Value[] {
                new SingleTagValue(Tag.PatientID),
                new ConstantValue("Sample (\\w+)"),
                new IntegerValue("1")
        })).on(o4));
    }
    
    public void testApplyNullValue() throws ScriptEvaluationException,IOException {
        final ScriptFunction match = new Match();
        assertNull(match.apply(Arrays.asList(new Value[] {
                new ConstantValue(null),
                new ConstantValue(".*_(v[0-9]+)_.*"),
                new IntegerValue("0")
        })).on(context));
    }
}
