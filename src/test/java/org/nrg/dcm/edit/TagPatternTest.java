/**
 * Copyright (c) 2012 Washington University
 */
package org.nrg.dcm.edit;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import com.google.common.io.Resources;
import junit.framework.TestCase;

import org.dcm4che2.data.DicomObject;
import org.nrg.dcm.DicomUtils;

import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;

/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public class TagPatternTest extends TestCase {
    private static final File f4;

    static {
        try {
            f4 = new File(Resources.getResource("dicom/1.MR.head_DHead.4.1.20061214.091206.156000.1632817982.dcm.gz").toURI());
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }

    public void testApplyInt() throws ScriptEvaluationException {
        final ElementMatcher tp1 = new TagPattern("(XXX#,XXXX)");
        assertTrue(tp1.apply(0x00010000));
        assertTrue(tp1.apply(0x00010001));
        assertFalse(tp1.apply(0x00000000));
        assertFalse(tp1.apply(0x00000001));
        assertTrue(tp1.apply(0x00190010));
        assertFalse(tp1.apply(0x00080005));

        final ElementMatcher tp2 = new TagPattern("(001@,XXXX)");
        assertTrue(tp2.apply(0x00101234));
        assertTrue(tp2.apply(0x00120000));
        assertTrue(tp2.apply(0x0014ffff));
        assertTrue(tp2.apply(0x0016fffe));
        assertTrue(tp2.apply(0x001e0101));
        assertFalse(tp2.apply(0x00110000));
        assertFalse(tp2.apply(0x10101234));
        assertFalse(tp2.apply(0x00200000));

        final ElementMatcher tp3 = new TagPattern("(FFFC,FFFC)");
        assertTrue(tp3.apply(0xfffcfffc));
    }

    public void testExtantConcreteTag() throws ScriptEvaluationException,IOException {
        final DicomObject o = DicomUtils.read(f4);
        final TagPattern tp = new TagPattern("(0010,0020)");
        final int[] r = Iterables.getOnlyElement(tp.apply(o));
        assertEquals(1, r.length);
        assertEquals(0x00100020, r[0]);		
    }

    public void testNonextantConcreteTag() throws ScriptEvaluationException,IOException {
        final DicomObject o = DicomUtils.read(f4);		
        final TagPattern tp = new TagPattern("(0010,0027)");
        final int[] r = Iterables.getOnlyElement(tp.apply(o));
        assertEquals(1, r.length);
        assertEquals(0x00100027, r[0]);	
    }

    public void testEvenPatternTags() throws ScriptEvaluationException,IOException {
        final DicomObject o = DicomUtils.read(f4);		
        final TagPattern tp = new TagPattern("(0010,00@0)");
        final List<int[]> r = Lists.newArrayList(tp.apply(o));
        assertEquals(2, r.size());
        assertEquals(1, r.get(0).length);
        assertEquals(0x00100020, r.get(0)[0]);
        assertEquals(1, r.get(1).length);
        assertEquals(0x00100040, r.get(1)[0]);		
    }

    public void testOddPatternTags() throws ScriptEvaluationException,IOException {
        final DicomObject o = DicomUtils.read(f4);		
        final TagPattern tp = new TagPattern("(0010,00#0)");
        final List<int[]> r = Lists.newArrayList(tp.apply(o));
        assertEquals(2, r.size());
        assertEquals(1, r.get(0).length);
        assertEquals(0x00100010, r.get(0)[0]);
        assertEquals(1, r.get(1).length);
        assertEquals(0x00100030, r.get(1)[0]);
    }

    public void testNonextantWildcard() throws ScriptEvaluationException,IOException {
        final DicomObject o = DicomUtils.read(f4);		
        final TagPattern tp = new TagPattern("(0011,33X0)");
        assertTrue(Iterables.isEmpty(tp.apply(o)));
    }

    public void testGetTopTag() throws ScriptEvaluationException {
        final ElementMatcher tp1 = new TagPattern("(0010,0020)");	// exact tag
        assertEquals(0x00100020, tp1.getTopTag());
        final ElementMatcher tp2 = new TagPattern("(0010,00@0)");	// even tag
        assertEquals(0x001000e0, tp2.getTopTag());
        final ElementMatcher tp3 = new TagPattern("(0010,00#0)");	// odd tag
        assertEquals(0x001000f0, tp3.getTopTag());	
        final ElementMatcher tp4 = new TagPattern("(00X0,00#0)");	// any+even tag
        assertEquals(0x00f000f0, tp4.getTopTag());
        final ElementMatcher tp5 = new TagPattern("(XXX#,XXXX)"); // private tag
        final long top = tp5.getTopTag();
        assertEquals(0xffffffffL, top);
    }
}
