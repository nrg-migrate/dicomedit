/**
 * Copyright (c) 2009-2012 Washington University
 */
package org.nrg.dcm.edit;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.Set;

import com.google.common.io.Resources;
import junit.framework.TestCase;

import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.Tag;
import org.dcm4che2.data.VR;
import org.dcm4che2.util.UIDUtils;
import org.nrg.dcm.DicomUtils;

import com.google.common.base.Function;
import com.google.common.collect.Sets;

/**
 * The test scripts here are based on the script language documentation at:
 * http://nrg.wustl.edu/projects/DICOM/AnonScript.jsp
 * since that's the closest thing to a spec that exists.
 * Note that this package uses MessageFormat instead of Formatter, so the
 * formatting string syntax has changed from the original DicomBrowser code.
 * 
 * @author Kevin A. Archie <karchie@wustl.edu>
 */
public class ScriptApplicatorTest extends TestCase {
    private static final File f4;
    private static final File f5;
    private static final File f6;

    static {
        try {
            f4 = new File(Resources.getResource("dicom/1.MR.head_DHead.4.1.20061214.091206.156000.1632817982.dcm.gz").toURI());
            f5 = new File(Resources.getResource("dicom/1.MR.head_DHead.5.1.20061214.091206.156000.0972418693.dcm.gz").toURI());
            f6 = new File(Resources.getResource("dicom/1.MR.head_DHead.6.1.20061214.091206.156000.2130219399.dcm.gz").toURI());
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }

    private static final String ORIG_INSITUTION = "Hospital";
    private static final String NEW_INSTITUTION = "My Test Institution";

    private static final String S_ASSIGN = "(0008,0080) := \"" + NEW_INSTITUTION + "\"\n";
    private static final String S_DELETE = "- (0010,0020)\n";

    private static final String S_COND_EQ = "(0020,0011) = \"5\" : (0008,103E) := \"Series Five\"\n"
        + "lowercase[(0020,0011)] = \"6\" : (0008,103E) := \"Series Six\"\n"
        + "(0020,0011) = \"4\" : - (0008,103E)\n";
    private static final int[] S_COND_EQ_TAG_INTS = { 0x00200011, 0x0008103e };
    private static final Set<Integer> S_COND_EQ_TAGS = Sets.newTreeSet();
    static {
        for (int i = 0; i < S_COND_EQ_TAG_INTS.length; i++) {
            S_COND_EQ_TAGS.add(Integer.valueOf(S_COND_EQ_TAG_INTS[i]));
        }
    }

    private static final String S_COND_RE = "(0020,0011) ~ \"[45]\" : (0008,103e) := \"Four or five: {0}\" (0020,0011)\n"
        + "(0020,0011) ~ \"[6-9]\" : (0008,103e) := \"Six through nine: {0}\" (0020,0011)\n";

    private static final String S_PRECEDENCE = "(0020,0011) = \"4\" : (0008,103e) := \"Series Four\"\n"
        + "(0008,103e) := \"Some other series\"\n";

    private static final String S_MULTILINE = "(0008,103e) :=\\\n\"foo\"\n";

    private static final String S_COMMENT = "// (0008,103e) := \"foo\"\n(0008,103e) := \"bar\"\n";

    private static final String S_USE_VAR = "(0008,103e) := studyDescription\n";

    private static final String S_USE_SUBSTR = "(0010,0020) := \"{0}_{1}\" (0010,0020), substring[(0008,0020),2,8]\n";

    private static final String S_NEW_UID = "(0020,000E) := new UID\n";

    private static final String S_INIT_VAR = "studyDescription := \"var init\"\n";

    private static final String S_USE_FORMAT = "(0008,103e) := format[\"{0}_{1}\", (0008,103e), \"extended\"]\n";

    private static final String S_USE_REPLACE = "(0008,103e) := replace[(0008,103e), \"_\", \":\"]\n";

    private static final String S_USE_HASHUID = "(0020,000d) := hashUID[(0020,000d)]\n";

    private static final String S_DESCRIPTION = "describe foo \"Description\"\nbar := foo\n";

    private static final String S_EXPORT_FIELD = "export foo \"baz:/my/export/path\"\nbar := foo\n";

    private static final String S_HIDDEN = "foo := \"bar\"\ndescribe foo hidden\n";
    
    private static final String S_INIT_VAR_FROM_TAG = "patientID := (0010,0020)\n";
    
    private static final String S_USE_FORMAT_FROM_VAR = "(0008,1030) := format[\"{0}_{1}\", (0008,1030), patientID]\n";
    
    private static final String S_REMOVE_PRIVATE = "-(XXX#,XXXX)\n";
    
    private static final String S_RETAIN_SOME_PRIVATE = "(0029,0010) = \"SIEMENS CSA HEADER\" : (0029,0010) := (0029,0010)\n"
            + "(0029,0010) = \"SIEMENS CSA HEADER\" : (0029,1010) := (0029,1010)\n"
            + "-(XXX#,XXXX)\n";
    
    private static final String S_REMOVE_SINGIDX_NESTED = "-(0008,1140)[1](0008,1155)\n";
    
    private static final String S_REMOVE_MULTIDX_NESTED = "-(0008,1140)[X](0008,1155)\n";
    
    private static final String S_REMOVE_NESTED = "-*(0008,1155)\n"; // "-*(0008,1155)\n";
 
    private static final Function<File,DicomObject> loader = new Function<File,DicomObject>() {
        public DicomObject apply(final File f) {
            try {
                return DicomUtils.read(f);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    };


    private ByteArrayInputStream bytes(final String s) {
        return new ByteArrayInputStream(s.getBytes());
    }

    /**
     * Test method for {@link org.nrg.dcm.edit.ScriptApplicator#ScriptApplicator(java.io.InputStream)}.
     * @throws Exception When something goes wrong.
     */
    public void testScriptApplicator() throws Exception {
        final ByteArrayInputStream bais1 = bytes(S_ASSIGN);
        new ScriptApplicator(bais1);
    }

    /**
     * Test method for {@link org.nrg.dcm.edit.ScriptApplicator#apply(File, DicomObject)}.
     */
    public void testApplyFile() throws Exception {
        final DicomObject do4 = loader.apply(f4);
        //	final DicomObject do5 = (DicomObject)loader.apply(f5);
        final DicomObject do6 = loader.apply(f6);

        final ScriptApplicator s_assign = new ScriptApplicator(bytes(S_ASSIGN));
        final DicomObject do6_assign = s_assign.apply(f6);

        assertEquals(ORIG_INSITUTION, do6.getString(Tag.InstitutionName));
        assertEquals(NEW_INSTITUTION, do6_assign.getString(Tag.InstitutionName));
        assertTrue(do6_assign.contains(0x00100020));

        final ScriptApplicator s_cond_eq = new ScriptApplicator(bytes(S_COND_EQ));
        final DicomObject do4_cond_eq = s_cond_eq.apply(f4);
        final DicomObject do5_cond_eq = s_cond_eq.apply(f5);
        final DicomObject do6_cond_eq = s_cond_eq.apply(f6);

        assertTrue(do4.contains(Tag.SeriesDescription));
        assertFalse(do4_cond_eq.contains(Tag.SeriesDescription));
        assertEquals("Series Five", do5_cond_eq.getString(Tag.SeriesDescription));
        assertEquals("Series Six", do6_cond_eq.getString(Tag.SeriesDescription));

        final ScriptApplicator s_cond_re = new ScriptApplicator(bytes(S_COND_RE));
        final DicomObject do4_cond_re = s_cond_re.apply(f4);
        final DicomObject do5_cond_re = s_cond_re.apply(f5);
        final DicomObject do6_cond_re = s_cond_re.apply(f6);

        assertEquals("Four or five: 4", do4_cond_re.getString(Tag.SeriesDescription));
        assertEquals("Four or five: 5", do5_cond_re.getString(Tag.SeriesDescription));
        assertEquals("Six through nine: 6", do6_cond_re.getString(Tag.SeriesDescription));

        final ScriptApplicator s_precedence = new ScriptApplicator(bytes(S_PRECEDENCE));
        final DicomObject do4_prec = s_precedence.apply(f4);
        final DicomObject do6_prec = s_precedence.apply(f6);

        assertEquals("Series Four", do4_prec.getString(Tag.SeriesDescription));
        assertEquals("Some other series", do6_prec.getString(Tag.SeriesDescription));

        final ScriptApplicator s_multiline = new ScriptApplicator(bytes(S_MULTILINE));
        final DicomObject do4_multiline = s_multiline.apply(f4);
        assertEquals("foo", do4_multiline.getString(Tag.SeriesDescription));

        final ScriptApplicator s_comment = new ScriptApplicator(bytes(S_COMMENT));
        final DicomObject do4_comment = s_comment.apply(f4);
        assertEquals("bar", do4_comment.getString(Tag.SeriesDescription));

        final ScriptApplicator s_use_variable = new ScriptApplicator(bytes(S_USE_VAR));
        final Variable studyDescVar = s_use_variable.getVariable("studyDescription");
        studyDescVar.setValue("my study");
        final DicomObject do5_use_variable = s_use_variable.apply(f5);
        assertEquals("my study", do5_use_variable.getString(Tag.SeriesDescription));

        final ScriptApplicator s_use_substr = new ScriptApplicator(bytes(S_USE_SUBSTR));
        final DicomObject do4_use_substr = s_use_substr.apply(f4);
        assertEquals(do4.getString(Tag.PatientID) + "_" + do4.getString(Tag.StudyDate).substring(2,8),
                do4_use_substr.getString(Tag.PatientID));


        final ScriptApplicator s_new_uid = new ScriptApplicator(bytes(S_NEW_UID));
        final DicomObject do4_new_uid = s_new_uid.apply(f4);
        assertEquals(do4.getString(Tag.StudyInstanceUID), do4_new_uid.getString(Tag.StudyInstanceUID));
        assertFalse(do4.getString(Tag.SeriesInstanceUID).equals(do4_new_uid.getString(Tag.SeriesInstanceUID)));

        final ScriptApplicator s_init_var = new ScriptApplicator(bytes(S_INIT_VAR + S_USE_VAR));
        final DicomObject do6_init_var = s_init_var.apply(f6);
        assertEquals("var init", do6_init_var.getString(Tag.SeriesDescription));

        final ScriptApplicator s_use_format = new ScriptApplicator(bytes(S_USE_FORMAT));
        final DicomObject do4_use_format = s_use_format.apply(f4);
        assertEquals("t1_mpr_1mm_p2_pos50_extended", do4_use_format.getString(Tag.SeriesDescription));

        final ScriptApplicator s_use_replace = new ScriptApplicator(bytes(S_USE_REPLACE));
        final DicomObject do4_use_replace = s_use_replace.apply(f4);
        assertEquals("t1:mpr:1mm:p2:pos50", do4_use_replace.getString(Tag.SeriesDescription));

        final ScriptApplicator s_use_hashUID = new ScriptApplicator(bytes(S_USE_HASHUID));
        final DicomObject do4_use_hashUID = s_use_hashUID.apply(f4);
        assertTrue(UIDUtils.isValidUID(do4_use_hashUID.getString(Tag.StudyInstanceUID)));
        assertFalse(do4.getString(Tag.StudyInstanceUID).equals(do4_use_hashUID.getString(Tag.StudyInstanceUID)));
    }
    
    public void testDeleteSingleTag() throws Exception {
        final DicomObject do6 = loader.apply(f6);
        assertTrue(do6.contains(0x00100020));
        final ScriptApplicator s_delete = new ScriptApplicator(bytes(S_DELETE));
        final DicomObject do1_delete = s_delete.apply(f6);
        assertFalse(do1_delete.contains(0x00100020));        
    }

    public void testGetSortedVariables() throws Exception {
        final String script = "a := \"foo\"\nb := format[\"{}_{}\", a, c]\nc := \"baz\"\n";
        final ScriptApplicator applicator = new ScriptApplicator(bytes(script));
        assertEquals(3, applicator.getSortedVariables().size());
        assertEquals("b", ((Variable)applicator.getSortedVariables().get(2)).getName());
        assertEquals(2, applicator.getSortedVariables(new String[]{"b"}).size());
        assertEquals(1, applicator.getSortedVariables(Arrays.asList(new String[]{"a", "b"})).size());
        assertEquals("c", ((Variable)applicator.getSortedVariables(Arrays.asList(new String[]{"a", "b"})).get(0)).getName());
    }

    public void testDescription() throws Exception {
        final ScriptApplicator applicator = new ScriptApplicator(bytes(S_DESCRIPTION));
        final Variable foo = applicator.getVariable("foo");
        assertEquals("Description", foo.getDescription());
        final Variable bar = applicator.getVariable("bar");
        assertNull(bar.getDescription());
    }

    public void testExportField() throws Exception {
        final ScriptApplicator applicator = new ScriptApplicator(bytes(S_EXPORT_FIELD));
        final Variable foo = applicator.getVariable("foo");
        assertEquals("baz:/my/export/path", foo.getExportField());
        final Variable bar = applicator.getVariable("bar");
        assertNull(bar.getExportField());   
    }

    public void testHidden() throws Exception {
        final ScriptApplicator applicator = new ScriptApplicator(bytes(S_HIDDEN));
        final Variable foo = applicator.getVariable("foo");
        assertTrue(foo.isHidden());
        final ScriptApplicator a2 = new ScriptApplicator(bytes(S_DESCRIPTION));
        final Variable f2 = a2.getVariable("foo");
        assertFalse(f2.isHidden());
    }
    
    public void testUnify() throws Exception {
        final ScriptApplicator a1 = new ScriptApplicator(bytes(S_INIT_VAR));
        final ScriptApplicator a2 = new ScriptApplicator(bytes(S_USE_VAR));
        final Variable a1StudyDesc = a1.getVariable("studyDescription");
        final Variable a2StudyDesc = a2.getVariable("studyDescription");
        assertNotSame(a2StudyDesc, a1StudyDesc);
        assertNull(a2StudyDesc.getInitialValue());
        a2.unify(a1StudyDesc);
        assertSame(a1.getVariable("studyDescription"), a2.getVariable("studyDescription"));
    }
    
    public void testUnifyNestedValues() throws Exception {
        final ScriptApplicator a1 = new ScriptApplicator(bytes(S_INIT_VAR_FROM_TAG));
        final ScriptApplicator a2 = new ScriptApplicator(bytes(S_USE_FORMAT_FROM_VAR));
        
        final DicomObject do4 = loader.apply(f4);
        assertEquals("head^DHead", do4.getString(Tag.StudyDescription));
        assertEquals("head^DHead_null", a2.apply(f4).getString(Tag.StudyDescription));
        
        for (final Variable v : a1.getVariables().values()) {
            a2.unify(v);
        }
        assertEquals("head^DHead_Sample ID", a2.apply(f4).getString(Tag.StudyDescription));
    }
    
    public void testRemovePrivate() throws Exception {
        final ScriptApplicator a1 = new ScriptApplicator(bytes(S_REMOVE_PRIVATE));
        final DicomObject do4 = loader.apply(f4);
        assertTrue(do4.contains(0x00185100));
        assertTrue(do4.contains(0x00190010));
        assertTrue(do4.contains(0x00191008));
        assertTrue(do4.contains(0x00191009));
        assertTrue(do4.contains(0x00290010));
        final DicomObject do4a = a1.apply(f4);
        assertTrue(do4a.contains(0x00185100));
        assertFalse(do4a.contains(0x00190010));
        assertFalse(do4a.contains(0x00191008));
        assertFalse(do4a.contains(0x00191009));
        assertFalse(do4a.contains(0x00290010));
    }
    
    public void testRetainPrivate() throws Exception {
        final ScriptApplicator a1 = new ScriptApplicator(bytes(S_RETAIN_SOME_PRIVATE));
        final DicomObject do4 = loader.apply(f4);
        assertTrue(do4.contains(0x00185100));
        assertTrue(do4.contains(0x00190010));
        assertTrue(do4.contains(0x00290010));
        assertEquals(VR.LO, do4.get(0x00290010).vr());
        assertTrue(do4.contains(0x00291008));
        assertTrue(do4.contains(0x00291010));
        assertEquals(VR.OB, do4.get(0x00291010).vr());
        final String creator = do4.getString(0x00290010);
        final byte[] obcontent = do4.getBytes(0x00291010);
        assertTrue(do4.contains(0x00291160));
        final DicomObject do4a = a1.apply(f4);
        assertTrue(do4a.contains(0x00185100));
        assertFalse(do4a.contains(0x00190010));
        assertTrue(do4a.contains(0x00290010));
        assertEquals(VR.LO, do4a.get(0x00290010).vr());
        assertEquals(creator, do4a.getString(0x00290010));
        assertFalse(do4a.contains(0x00291008));
        assertTrue(do4a.contains(0x00291010));
        assertEquals(VR.OB, do4a.get(0x00291010).vr());
        assertTrue(Arrays.equals(obcontent, do4a.getBytes(0x00291010)));
        assertFalse(do4a.contains(0x00291160));
    }
    
    public void testRemoveNestedByIndex() throws Exception {
        final ScriptApplicator a1 = new ScriptApplicator(bytes(S_REMOVE_SINGIDX_NESTED));
        final DicomObject do4 = loader.apply(f4);
        assertNotNull(do4.get(new int[]{0x00081140,0,0x00081155}));
        assertNotNull(do4.get(new int[]{0x00081140,1,0x00081155}));
        assertNotNull(do4.get(new int[]{0x00081140,2,0x00081155}));
        assertNull(do4.get(new int[]{0x00081140,3,0x00081155}));
        final DicomObject do4a = a1.apply(f4);
        assertNotNull(do4a.get(new int[]{0x00081140,0,0x00081155})); // SQ item 0 untouched
        assertNotNull(do4a.get(new int[]{0x00081140,0,0x00081150})); // SQ item 1 still present
        assertNull(do4a.get(new int[]{0x00081140,1,0x00081155}));   // but matching elements removed
        assertNotNull(do4a.get(new int[]{0x00081140,2,0x00081155})); // SQ item 2 untouched
        assertNull(do4a.get(new int[]{0x00081140,3,0x00081155}));
     }
    
    public void testRemoveMultIndex() throws Exception {
        final ScriptApplicator a1 = new ScriptApplicator(bytes(S_REMOVE_MULTIDX_NESTED));
        final DicomObject do4 = loader.apply(f4);
        assertNotNull(do4.get(new int[]{0x00081140,0,0x00081155}));
        assertNotNull(do4.get(new int[]{0x00081140,1,0x00081155}));
        assertNotNull(do4.get(new int[]{0x00081140,2,0x00081155}));
        assertNull(do4.get(new int[]{0x00081140,3,0x00081155}));
        final DicomObject do4a = a1.apply(f4);
        assertNotNull(do4a.get(new int[]{0x00081140,0,0x00081150})); // SQ item 0 still present
        assertNull(do4a.get(new int[]{0x00081140,0,0x00081155}));   // but matching elements removed
        assertNull(do4a.get(new int[]{0x00081140,1,0x00081155}));
        assertNull(do4a.get(new int[]{0x00081140,2,0x00081155}));
        assertNull(do4a.get(new int[]{0x00081140,3,0x00081155}));
     }
    
    public void testRemoveNested() throws Exception {
        final ScriptApplicator a1 = new ScriptApplicator(bytes(S_REMOVE_NESTED));
        final DicomObject do4 = loader.apply(f4);
        assertNotNull(do4.get(new int[]{0x00081140,0,0x00081155}));
        assertNotNull(do4.get(new int[]{0x00081140,1,0x00081155}));
        assertNotNull(do4.get(new int[]{0x00081140,2,0x00081155}));
        assertNull(do4.get(new int[]{0x00081140,3,0x00081155}));
        final DicomObject do4a = a1.apply(f4);
        assertNotNull(do4a.get(new int[]{0x00081140,0,0x00081150})); // SQ item 0 still present
        assertNull(do4a.get(new int[]{0x00081140,0,0x00081155}));   // but matching elements removed
        assertNull(do4a.get(new int[]{0x00081140,1,0x00081155}));
        assertNull(do4a.get(new int[]{0x00081140,2,0x00081155}));
        assertNull(do4a.get(new int[]{0x00081140,3,0x00081155}));
     }

}
