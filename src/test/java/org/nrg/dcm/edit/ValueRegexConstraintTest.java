/**
 * Copyright (c) 2012 Washington University
 */
package org.nrg.dcm.edit;

import org.dcm4che2.data.BasicDicomObject;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.Tag;
import org.dcm4che2.data.VR;

import junit.framework.TestCase;

/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public class ValueRegexConstraintTest extends TestCase {
    /**
     * Test method for {@link org.nrg.dcm.edit.AbstractValueConstraint#matches(org.dcm4che2.data.DicomObject)}.
     */
    public void testMatchesDicomObject() throws ScriptEvaluationException {
        final DicomObject o1 = new BasicDicomObject();
        o1.putString(Tag.StudyDescription, VR.LO, "foo");
        
        final DicomObject o2 = new BasicDicomObject();
        o2.putString(Tag.StudyDescription, VR.LO, "bar");
        
        final Value studyDesc = new SingleTagValue(Tag.StudyDescription);
        final ConstraintMatch m1 = new ValueRegexConstraint(studyDesc, new ConstantValue("f\\wo"));
        assertTrue(m1.matches(o1));
        assertFalse(m1.matches(o2));
        
        final ConstraintMatch m3 = new ValueRegexConstraint(studyDesc, new ConstantValue("\\w+"));
        assertTrue(m3.matches(o1));
        assertTrue(m3.matches(o2));
        
        final Value nullVal = new ConstantValue(null);       
        final ConstraintMatch m4 = new ValueRegexConstraint(nullVal, new ConstantValue(""));
        assertFalse(m4.matches(o1));
        
        final Value patientName = new SingleTagValue(Tag.PatientName);
        final ConstraintMatch m5 = new ValueRegexConstraint(patientName, new ConstantValue("\\w+"));
        assertFalse(m5.matches(o1));
    }
}
