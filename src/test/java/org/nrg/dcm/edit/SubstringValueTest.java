/**
 * Copyright (c) 2012 Washington University
 */
package org.nrg.dcm.edit;

import java.util.Map;

import org.dcm4che2.data.BasicDicomObject;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.Tag;
import org.dcm4che2.data.VR;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.ImmutableSortedSet;

import junit.framework.TestCase;

/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public class SubstringValueTest extends TestCase {

    /**
     * Test method for {@link org.nrg.dcm.edit.SubstringValue#getTags()}.
     */
    public void testGetTags() {
        final Value v1 = new SingleTagValue(Tag.PatientName);
        final Value ss1 = new SubstringValue(v1, 0, 1);
        assertEquals(ImmutableSortedSet.of(0xffffffffL & Tag.PatientName), ss1.getTags());
    }

    /**
     * Test method for {@link org.nrg.dcm.edit.SubstringValue#getVariables()}.
     */
    public void testGetVariables() {
        final Value v1 = new SingleTagValue(Tag.PatientName);
        final Value ss1 = new SubstringValue(v1, 0, 1);
        assertTrue(ss1.getVariables().isEmpty());

        final Variable var = new ScriptReferencedVariable("foo");
        final Value v2 = new VariableValue(var);
        final Value ss2 = new SubstringValue(v2, 0, 1);
        assertEquals(ImmutableSet.of(var), ss2.getVariables());
    }

    /**
     * Test method for {@link org.nrg.dcm.edit.SubstringValue#on(org.dcm4che2.data.DicomObject)}.
     */
    public void testOnDicomObject() throws ScriptEvaluationException {
        final Value v1 = new SingleTagValue(Tag.PatientName);
        final DicomObject o = new BasicDicomObject();
        o.putString(Tag.PatientName, VR.PN, "abcdef");
        final Value ss = new SubstringValue(v1, 1, 4);
        assertEquals("bcd", ss.on(o));
        
        o.putString(Tag.PatientName, VR.PN, "abc");
        try {
            ss.on(o);
            fail("expected string index bounds exception");
        } catch (ScriptEvaluationException ok) {}
    }

    /**
     * Test method for {@link org.nrg.dcm.edit.SubstringValue#on(java.util.Map)}.
     */
    public void testOnMapOfIntegerString() throws ScriptEvaluationException {
        final Value v1 = new SingleTagValue(Tag.PatientName);
        final Map<Integer,String> m = ImmutableMap.of(Tag.PatientName, "abcdef");
        final Value ss = new SubstringValue(v1, 2, 5);
        assertEquals("cde", ss.on(m));
        
        try {
            ss.on(ImmutableMap.of(Tag.PatientName, "abcd"));
            fail("expected string index bounds exception");
        } catch (ScriptEvaluationException ok) {}
    }
}
