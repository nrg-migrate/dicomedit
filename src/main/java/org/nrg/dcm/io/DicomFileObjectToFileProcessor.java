/**
 * Copyright (c) 2009,2011 Washington University
 */
package org.nrg.dcm.io;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import org.dcm4che2.data.DicomObject;
import org.dcm4che2.io.DicomOutputStream;
import org.nrg.dcm.edit.DicomFileObjectProcessor;

import com.google.common.base.Function;


/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public final class DicomFileObjectToFileProcessor implements DicomFileObjectProcessor {
    private final Function<File,File> fn;

    /**
     * Saves each given DicomObject o with associated File f to some File f'
     * determined by a provided Function operating on the original File f.
     * The Iterable argument to reduce() should have as its first element
     * the original File f, and has its second element the DicomObject o.
     */
    public DicomFileObjectToFileProcessor(final Function<File,File> fn) {
        this.fn = fn;
    }

    public Object process(final File f, final DicomObject o) throws IOException {
        final File out = fn.apply(f);
        final FileOutputStream fos = new FileOutputStream(out);
        IOException ioexception = null;
        try {
            final DicomOutputStream dos = new DicomOutputStream(fos);
            try {
                dos.writeDicomFile(o);
                return out;
            } catch (IOException e) {
                throw ioexception = e;
            } finally {
                try {
                    dos.close();
                } catch (IOException e) {
                    throw ioexception = null == ioexception ? e : ioexception;
                }
            }
        } finally {
            try {
                fos.close();
            } catch (IOException e) {
                throw null == ioexception ? e : ioexception;
            }
        }
    }
}
