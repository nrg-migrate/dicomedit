/**
 * Copyright (c) 2009-2011 Washington University
 */
package org.nrg.dcm.io;

import java.io.File;
import java.net.URI;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.dcm4che2.data.DicomObject;
import org.nrg.dcm.DicomUtils;
import org.nrg.dcm.edit.Action;
import org.nrg.dcm.edit.Statement;
import org.nrg.util.CanceledOperationException;
import org.nrg.util.EditProgressMonitor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public final class BatchExporter implements Runnable {
    private static final Exception canceledException = new CanceledOperationException();
    private final Logger logger = LoggerFactory.getLogger(BatchExporter.class);
    private final DicomObjectExporter objectExporter;
    private final List<Statement> statements;
    private final Iterator<?> toExport;
    private EditProgressMonitor pm = null;
    private int progress = -1;
    private final Map<Object,Throwable> failures = Maps.newLinkedHashMap();
    private boolean isPending = true;

    public BatchExporter(final DicomObjectExporter objectExporter,
            final Iterable<Statement> statements, final Iterator<?> toExport) {
        this.objectExporter = objectExporter;
        this.statements = Lists.newArrayList(statements);
        Collections.reverse(this.statements);
        this.toExport = toExport;
    }

    public BatchExporter(final DicomObjectExporter objectExporter,
            final List<Statement> statements, final Iterable<?> toExport) {
        this(objectExporter, statements, toExport.iterator());
    }

    public void setProgressMonitor(final EditProgressMonitor pm, final int progress) {
        this.pm = pm;
        this.progress = progress;
    }

    /**
     * Checks the status of the export operation.
     * @return true if the operation is running or waiting to start; false if finished
     */
    public boolean isPending() { return isPending; }

    /**
     * Returns a description of which exportables failed and why; if empty, all data
     * were exported successfully.
     * @return Map Object -> Exception mapping exportables to the cause of export failure
     */
    public Map<?,Throwable> getFailures() {
        if (isPending) {
            throw new IllegalStateException("export not yet complete");
        } else {
            return failures;
        }
    }

    /**
     * Returns the progress monitor progress value.
     * @return the most recent argument to setProgress()
     */
    public int getProgress() { return progress; }

    public void run() {
        final EditProgressMonitor pm = this.pm;

        EXPORT_LOOP: while (toExport.hasNext())  {
            final Object t = toExport.next();
            if (null != pm) {
                if (pm.isCanceled()) {
                    failures.put(t, canceledException);
                    while (toExport.hasNext()) {
                        failures.put(toExport.next(), canceledException);
                    }
                    isPending = true;
                    return;
                } else {
                    pm.setNote(t.toString());
                }
            }
            logger.trace("Checking {}", t);

            final File f;
            final DicomObject o;
            try {
                if (t instanceof File) {
                    f = (File)t;
                    o = DicomUtils.read(f);
                } else if (t instanceof URI) {
                    final URI uri = (URI)t;
                    if ("file".equals(uri.getScheme())) {
                        f = new File(uri);
                        o = DicomUtils.read(f);
                    } else {
                        f = null;
                        o = DicomUtils.read(uri);
                    }
                } else if (t instanceof DicomObject) {
                    f = null;
                    o = (DicomObject)t;
                } else {
                    failures.put(t,
                            new IllegalArgumentException("cannot export class "
                                    + t.getClass().getName()));
                    continue EXPORT_LOOP;
                }
            } catch (Throwable e) {
                logger.debug("load failed for " + t, e);
                failures.put(t, e);
                continue EXPORT_LOOP;
            }

            try {
                for (final Action a : Statement.getActions(statements, f, o)) {
                    a.apply();
                }
            } catch (Throwable e) {
                logger.debug("script application failed for " + t, e);
                failures.put(t, e);
            }

            try {
                logger.trace("exporting {}", t);
                objectExporter.export(o, f);
                progress++;
                if (null != pm) {
                    pm.setProgress(progress);
                }
            } catch (Throwable e) {
                logger.debug("export failed for " + t, e);
                failures.put(t, e);
            }
        }

        objectExporter.close();
        if (null != pm) {
            pm.close();
        }

        logger.debug("Export completed");
        if (!failures.isEmpty()) {
            logger.debug("Failures: {}", failures);
        }
        isPending = false;
        
    }
}
