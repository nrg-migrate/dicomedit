/**
 * Copyright (c) 2009 Washington University
 */
package org.nrg.dcm.io;

import java.io.File;

import org.dcm4che2.data.DicomObject;

/**
 * @author Kevin A. Archie <karchie@npg.wustl.edu>
 *
 */
public interface DicomObjectExporter {
    void close();
    void export(DicomObject o, File source) throws Exception;
}
