/**
 * Copyright (c) 2006-2013 Washington University School of Medicine
 */
package org.nrg.dcm.edit;

import java.text.MessageFormat;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;

import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.VR;
import org.dcm4che2.util.TagUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Objects;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;


/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 */
public class Assignment extends AbstractOperation {
    private final Logger logger = LoggerFactory.getLogger(Assignment.class);
    private final ElementMatcher pattern;
    private final Value value;

    public Assignment(final ElementMatcher tagpat, final Value value) {
        super("Assign");
        this.pattern = tagpat;
        this.value = value;
    }

    public Assignment(final int tag, final Value value) {
        this(new ExactTag(tag), value);
    }

    public Assignment(final Integer tag, final Value value) {
        this(tag.intValue(), value);
    }

    public Assignment(final int tag, final String svalue) {
        this(tag, new ConstantValue(svalue));
    }

    /*
     * (non-Javadoc)
     * @see org.nrg.dcm.edit.Operation#affects(int)
     */
    public boolean affects(final int tag) {
        return pattern.apply(tag);
    }

    /*
     * (non-Javadoc)
     * @see org.nrg.dcm.edit.Operation#getTopTag()
     */
    public long getTopTag() {
        final SortedSet<Long> tags = Sets.newTreeSet(value.getTags());
        tags.add(pattern.getTopTag());
        return tags.last();
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    public boolean equals(Object o) {
        if (o instanceof Assignment) {
            final Assignment oa = (Assignment)o;
            return pattern.equals(oa.pattern) && value.equals(oa.value);
        } else {
            return false;
        }
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    public int hashCode() {
        return Objects.hashCode(pattern, value);
    }

    private abstract class TagAssignment {
        final int[] tag;
        final VR vr;

        TagAssignment(final int[] tag, final VR vr) {
            this.tag = tag;
            this.vr = vr;
        }

        public abstract void applyTo(DicomObject o) throws ScriptEvaluationException;
    }

    // TODO: (maybe) push through SQ that have been removed
    private class TagBytesAssignment extends TagAssignment {
        final byte[] bytes;

        TagBytesAssignment(final int[] tag, final VR vr, final byte[] bytes) {
            super(tag, vr);
            this.bytes = bytes;
        }

        public void applyTo(final DicomObject o) throws ScriptEvaluationException {
            try {
                o.putBytes(tag, vr, bytes);
            } catch (UnsupportedOperationException e) {
                throw new ScriptEvaluationException("Unable to set attribute "
                        + TagUtils.toString(tag[tag.length-1]) + " (VR " + vr + ") to binary content", e);
            }    		
        }
    }

    private class TagStringAssignment extends TagAssignment {
        final String s;

        TagStringAssignment(final int[] tag, final VR vr, final String s) {
            super(tag, vr);
            this.s = s;
        }

        public void applyTo(final DicomObject o) throws ScriptEvaluationException {
            try {
                o.putString(tag, vr, s);
            } catch (UnsupportedOperationException e) {
                throw new ScriptEvaluationException("Unable to set attribute "
                        + TagUtils.toString(tag[tag.length-1]) + " (VR " + vr + ") to \"" + s + "\"", e);
            }
        }

    }

    /*
     * (non-Javadoc)
     * @see org.nrg.dcm.edit.Operation#makeAction(org.dcm4che2.data.DicomObject)
     */
    public Action makeAction(final DicomObject o) throws ScriptEvaluationException {
        final String vstr;
        final byte[] vbytes;
        final VR ovr = value.vrOn(o);
        if (VR.OB.equals(ovr) || VR.OW.equals(ovr) || VR.UN.equals(ovr)) {
            vbytes = value.bytesOn(o);
            vstr = null;
        } else {
            vbytes = null;
            vstr = value.on(o);
        }

        // Assignment is tricky with tag patterns. It's not enough just to compute
        // the rvalue on the premodification object; we need to determine the lvalues
        // premodification too.
        final List<TagAssignment> assigns = Lists.newArrayList();
        for (final int[] tag : pattern.apply(o)) {
            VR vr = o.vrOf(tag[tag.length-1]);
            if (VR.UN.equals(vr)) { // try the source value VR
                logger.debug("using source VR {} for nonstandard tag {}", ovr, TagUtils.toString(tag[tag.length-1]));
                vr = ovr;
            }
            if (null != vbytes) {
                assigns.add(new TagBytesAssignment(tag, vr, vbytes));
            } else if (null != vstr) {
                assigns.add(new TagStringAssignment(tag, vr, vstr));
            } else {
                logger.debug("skipping null value assignment for {}", TagUtils.toString(tag[tag.length-1]));
            }
        }

        return new Action() {
            public void apply() throws ScriptEvaluationException {
                for (final TagAssignment a : assigns) {
                    a.applyTo(o);
                }
            }

            public String toString() {
                return MessageFormat.format("{0}: {1} := {2}",
                        new Object[]{getName(), pattern, null == vstr ? Arrays.toString(vbytes) : vstr});
            }
        };
    }

    /*
     * (non-Javadoc)
     * @see org.nrg.dcm.edit.Operation#replace(org.nrg.dcm.edit.Variable)
     */
    public void replace(final Variable v) {
        value.replace(v);
    }

    /*
     * (non-Javadoc)
     * @see org.nrg.dcm.edit.Operation#apply(java.util.Map)
     */
    public String apply(final Map<Integer,String> vals) throws ScriptEvaluationException {
        return value.on(vals);
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    public String toString() {
        final StringBuffer sb = new StringBuffer(getName());
        sb.append(" ");
        sb.append(pattern);
        String v;
        try {
            v = value.on(new HashMap<Integer,String>());
        } catch (ScriptEvaluationException e) {
            v = value.toString();
        }
        if (null == v) {
            v = value.toString();
        }
        sb.append(" := ").append(v);
        return sb.toString();
    }
}
