/*
 * Copyright (c) 2006-2011 Washington University
 */
package org.nrg.dcm.edit;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.SortedSet;

import org.dcm4che2.data.DicomObject;
import org.nrg.dcm.DicomUtils;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

/**
 * 
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public class Statement {
    private final Constraint c;
    private final Operation op;

    Statement(final Operation a) {
        this(null, a);
    }

    public Statement(final Constraint c, final Operation a) {
        this.op = a;
        this.c = c;
    }

    public long getTopTag() {
       final SortedSet<Long> tags = Sets.newTreeSet();
       if (null != c) {
           tags.addAll(c.getTags());
       }
       tags.add(op.getTopTag());
       return tags.last();
    }
    
    public boolean isConstrained() { return null != c; }

    public boolean matchesConstraint(final File f, final DicomObject o)
    throws ScriptEvaluationException {
        return null == c || c.matches(f, o);
    }

    public Operation getOperation() { return op; }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    public String toString() {
        final StringBuilder sb = new StringBuilder(super.toString());
        sb.append(" ");
        if (c != null) {
            sb.append(c).append(" => ");
        }
        sb.append(op);    
        return sb.toString();
    }

    /**
     * For all contained Statements, get all Operations that
     * apply to the File f, with corresponding DicomObject o.
     * In many cases (mostly, if we have no constraints) we can get by
     * without o, so o=null is valid.  If o is null, the object is loaded
     * from file if it's needed.
     * @param f the File containing the object
     * @param o the corresponding DicomObject (can be null)
     * @return List of Operations applicable to the named File
     * @throws IOException
     */
    public static List<Operation> getOperations(final Iterable<Statement> statements,
            final File f, DicomObject o)
            throws IOException,ScriptEvaluationException {
        final List<Operation> ops = Lists.newArrayList();
        for (final Statement s : statements) {
            final Operation op = s.getOperation();
            if (null != op && s.isConstrained()) {
                o = DicomUtils.read(f);
                if (s.matchesConstraint(f, o)) {
                    ops.add(op);
                }
            } else {
                ops.add(op);
            }
        }
        return ops;
    }

    public static List<Operation> getOperations(final Iterable<Statement> s, final File f)
    throws IOException,ScriptEvaluationException {
        return getOperations(s, f, null);
    }

    public static List<Action> getActions(final Iterable<Statement> statements,
            final File f, final DicomObject o)
            throws AttributeException,ScriptEvaluationException {
        final List<Action> actions = Lists.newArrayList();
        for (final Statement s : statements) {
            if (s.matchesConstraint(f, o)) {
                final Operation operation = s.getOperation();
                if (null != operation) {
                    final Action action = operation.makeAction(o);
                    if (null != action) {
                        actions.add(action);
                    }
                }
            }
        }
        return actions;     
    }
}
