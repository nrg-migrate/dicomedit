/**
 * Copyright (c) 2009 Washington University
 */
package org.nrg.dcm.edit;

/**
 * @author Kevin A. Archie <karchie@npg.wustl.edu>
 *
 */
public class ScriptEvaluationException extends Exception {
    private static final long serialVersionUID = 1L;
    
    /**
     * @param message
     */
    public ScriptEvaluationException(final String message) {
	super(message);
    }

    /**
     * @param cause
     */
    public ScriptEvaluationException(final Throwable cause) {
	super(cause);
    }

    /**
     * @param message
     * @param cause
     */
    public ScriptEvaluationException(String message, Throwable cause) {
	super(message, cause);
    }
}
