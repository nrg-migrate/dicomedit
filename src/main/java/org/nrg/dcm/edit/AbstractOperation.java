/**
 * Copyright (c) 2006-2011 Washington University
 */
package org.nrg.dcm.edit;


/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public abstract class AbstractOperation implements Operation {
    private final String name;

    protected AbstractOperation(final String name) {
        this.name = name;
    }

    /*
     * (non-Javadoc)
     * @see org.nrg.dcm.edit.Operation#getName()
     */
    public String getName() { return name; }
}
