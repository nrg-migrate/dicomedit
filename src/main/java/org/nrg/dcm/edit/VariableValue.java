/**
 * Copyright (c) 2009-2011 Washington University
 */
package org.nrg.dcm.edit;

import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;

import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.VR;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.ImmutableSortedSet;

/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public final class VariableValue implements Value {
    private static final SortedSet<Long> EMPTY = ImmutableSortedSet.of();
    private final Logger logger = LoggerFactory.getLogger(VariableValue.class);
    private Variable variable;

    public VariableValue(final Variable variable) {
        this.variable = variable;
    }


    /* (non-Javadoc)
     * @see org.nrg.dcm.edit.Value#getTags()
     */
    public SortedSet<Long> getTags() { return EMPTY; }

    /*
     * (non-Javadoc)
     * @see org.nrg.dcm.edit.Value#getVariables()
     */
    public Set<Variable> getVariables() {
        return Collections.singleton(variable);
    }

    /* (non-Javadoc)
     * @see org.nrg.dcm.edit.Value#on(org.dcm4che2.data.DicomObject)
     */
    public String on(final DicomObject o) throws ScriptEvaluationException {
        logger.trace("evaluating " + this);
        final String v = variable.getValue();
        logger.trace("primary value = " + v);
        if (null == v) {
            final Value initial = variable.getInitialValue();
            logger.trace("initial value = " + initial);
            return null == initial ? null : initial.on(o);
        } else {
            return v;
        }
    }

    /* (non-Javadoc)
     * @see org.nrg.dcm.edit.Value#on(java.util.Map)
     */
    public String on(final Map<Integer,String> m) throws ScriptEvaluationException {
        final String v = variable.getValue();
        if (null == v) {
            final Value initial = variable.getInitialValue();
            return null == initial ? null : initial.on(m);
        } else {
            return v;
        }
    }

    /*
     * (non-Javadoc)
     * @see org.nrg.dcm.edit.Value#replaceVariable(org.nrg.dcm.edit.Variable)
     */
    public void replace(final Variable v) {
        if (v.equals(variable)) {
            variable = v;
        }
    }
    
    /*
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    public String toString() {
        final StringBuffer sb = new StringBuffer(super.toString());
        sb.append("(").append(variable).append(")");
        return sb.toString();
    }


    /* (non-Javadoc)
     * @see org.nrg.dcm.edit.Value#vr()
     */
    public VR vr() {
        final Value initial = variable.getInitialValue();
        return null == initial ? VR.UN : initial.vr();
    }


    /* (non-Javadoc)
     * @see org.nrg.dcm.edit.Value#vrOn(org.dcm4che2.data.DicomObject)
     */
    public VR vrOn(final DicomObject o) throws ScriptEvaluationException {
        final Value initial = variable.getInitialValue();
        return null == initial ? VR.UN : initial.vrOn(o);
    }


    /* (non-Javadoc)
     * @see org.nrg.dcm.edit.Value#bytesOn(org.dcm4che2.data.DicomObject)
     */
    public byte[] bytesOn(final DicomObject o) throws ScriptEvaluationException {
        final String s = on(o);
        return null == s ? null : s.getBytes();
    }
}
