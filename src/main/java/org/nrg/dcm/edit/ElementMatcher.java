package org.nrg.dcm.edit;

import org.dcm4che2.data.DicomObject;

import com.google.common.base.Function;

public interface ElementMatcher extends Function<DicomObject,Iterable<int[]>> {
    
    Iterable<int[]> apply(DicomObject o);
    
    boolean apply(Integer tag);
    
	/**
	 * Determine the largest tag value this pattern might represent.
	 * @return
	 */
	abstract long getTopTag();
}