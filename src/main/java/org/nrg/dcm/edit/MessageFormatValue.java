/**
 * Copyright (c) 2009-2013 Washington University School of Medicine
 */
package org.nrg.dcm.edit;

import java.text.MessageFormat;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;

import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.VR;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Objects;
import com.google.common.collect.ImmutableSortedSet;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public final class MessageFormatValue implements Value {
    private final Logger logger = LoggerFactory.getLogger(MessageFormatValue.class);
    private final Value format;
    private final List<Value> values;
    private final SortedSet<Long> tags;
    private final Set<Variable> variables;
    private final VR vr;

    public MessageFormatValue(final Value format, final Iterable<? extends Value> values) {
        this.format = format;
        this.values = Lists.newArrayList(values);

        // Sort out the dependencies
        final Set<Long> tags = Sets.newLinkedHashSet();
        variables = Sets.newLinkedHashSet();
        VR vr = VR.UN;
        for (final Value value : values) {
            if (!VR.UN.equals(vr)) {
                vr = value.vr();
            }
            tags.addAll(value.getTags());
            variables.addAll(value.getVariables());
        }
        this.tags = ImmutableSortedSet.copyOf(tags);
        this.vr = vr;
    }

    public MessageFormatValue(final String format, final Iterable<? extends Value> values) {
        this(new ConstantValue(format), values);
    }

    /*
     * (non-Javadoc)
     * @see org.nrg.dcm.edit.Value#getTags()
     */
    public SortedSet<Long> getTags() { return tags; }

    /*
     * (non-Javadoc)
     * @see org.nrg.dcm.edit.Value#getVariables()
     */
    public Set<Variable> getVariables() { return variables; }

    /* (non-Javadoc)
     * @see org.nrg.dcm.edit.Value#on(org.dcm4che2.data.DicomObject)
     */
    public String on(final DicomObject o) throws ScriptEvaluationException {
        final Object[] vals = new Object[values.size()];
        logger.trace("format values: {}", values);
        for (int i = 0; i < values.size(); i++) {
            vals[i] = ((Value)values.get(i)).on(o);
        }
        logger.trace("format args: {}", vals);
        return MessageFormat.format(format.on(o), vals);
    }

    /*
     * (non-Javadoc)
     * @see org.nrg.dcm.edit.Value#on(java.util.Map)
     */
    public String on(final Map<Integer,String> m) throws ScriptEvaluationException {
        final Object[] vals = new Object[values.size()];
        for (int i = 0; i < values.size(); i++) {
            final Value v = values.get(i);
            vals[i] = v.on(m);
        }
        return MessageFormat.format(format.on(m), vals);
    }

    public void replace(final Variable var) {
        format.replace(var);
        for (final Value val : values) {
            val.replace(var);
        }
        if (variables.contains(var)) {
            variables.remove(var);
            variables.add(var);
        }
    }
    
    /*
     * (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    public boolean equals(final Object o) {
        if (o instanceof MessageFormatValue) {
            final MessageFormatValue other = (MessageFormatValue)o;
            return format.equals(other.format) && values.equals(other.values);
        } else {
            return false;
        }
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    public int hashCode() {
        return Objects.hashCode(format, values);
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    public String toString() {
        final StringBuilder sb = new StringBuilder("format(\"");
        sb.append(format).append("\"");
        for (final Value v : values) {
            sb.append(", ").append(v);
        }
        sb.append(")");
        return sb.toString();
    }

    /* (non-Javadoc)
     * @see org.nrg.dcm.edit.Value#vr()
     */
    public VR vr() { return vr; }

    /* (non-Javadoc)
     * @see org.nrg.dcm.edit.Value#vrOn(org.dcm4che2.data.DicomObject)
     */
    public VR vrOn(DicomObject _) { return vr; }

    /* (non-Javadoc)
     * @see org.nrg.dcm.edit.Value#bytesOn(org.dcm4che2.data.DicomObject)
     */
    public byte[] bytesOn(final DicomObject o) throws ScriptEvaluationException {
        final String s = on(o);
        return null == s ? null : s.getBytes();
    }
}
