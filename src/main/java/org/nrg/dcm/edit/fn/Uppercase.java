/**
 * Copyright (c) 2010-2013 Washington University School of Medicine
 */
package org.nrg.dcm.edit.fn;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;

import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.VR;
import org.nrg.dcm.edit.ScriptEvaluationException;
import org.nrg.dcm.edit.ScriptFunction;
import org.nrg.dcm.edit.Value;
import org.nrg.dcm.edit.Variable;

/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public final class Uppercase implements ScriptFunction {
    public static final String name = "uppercase";

    /* (non-Javadoc)
     * @see org.nrg.dcm.edit.ScriptFunction#apply(java.util.List)
     */
    public Value apply(final List<? extends Value> args) throws ScriptEvaluationException {
        final Value v;
        try {
            v = args.get(0);
        } catch (IndexOutOfBoundsException e) {
            throw new ScriptEvaluationException(name + " requires one argument");
        }
        return new Value() {
            public String on(Map<Integer,String> m) throws ScriptEvaluationException {
                return toUpper(v.on(m));
            }

            public String on(DicomObject o) throws ScriptEvaluationException {
                return toUpper(v.on(o));
            }

            public Set<Variable> getVariables() { return v.getVariables(); }

            public SortedSet<Long> getTags() { return v.getTags(); }
            
            public void replace(final Variable var) {
                v.replace(var);
            }

            public VR vr() { return v.vr(); }

            public VR vrOn(final DicomObject o) throws ScriptEvaluationException {
                return v.vrOn(o);
            }

            public byte[] bytesOn(final DicomObject o) throws ScriptEvaluationException {
                final String s = on(o);
                return null == s ? null : s.getBytes();
            }
        };
    }

    private static String toUpper(final String s) {
        return null == s ? null : s.toUpperCase();
    }
}
