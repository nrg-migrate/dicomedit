/**
 * Copyright (c) 2010-2013 Washington University School of Medicine
 */
package org.nrg.dcm.edit.fn;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.VR;
import org.nrg.dcm.edit.IntegerValue;
import org.nrg.dcm.edit.ScriptEvaluationException;
import org.nrg.dcm.edit.ScriptFunction;
import org.nrg.dcm.edit.Value;
import org.nrg.dcm.edit.Variable;

import com.google.common.collect.Sets;

/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public final class Match implements ScriptFunction {
    public static final String name = "match";

    /* (non-Javadoc)
     * @see org.nrg.dcm.edit.ScriptFunction#apply(java.util.List)
     */
    public Value apply(List<? extends Value> args) throws ScriptEvaluationException {
        final Value value = args.get(0);
        final Value pattern = args.get(1);
        final int group = ((IntegerValue)args.get(2)).getValue();
        return new Value() {
            /*
             * (non-Javadoc)
             * @see org.nrg.dcm.edit.Value#getVariables()
             */
            public Set<Variable> getVariables() {
                final Set<Variable> vs = Sets.newLinkedHashSet(value.getVariables());
                vs.addAll(pattern.getVariables());
                return vs;
            }

            /*
             * (non-Javadoc)
             * @see org.nrg.dcm.edit.Value#getTags()
             */
            public SortedSet<Long> getTags() {
                final SortedSet<Long> ts = Sets.newTreeSet(value.getTags());
                ts.addAll(pattern.getTags());
                return ts;
            }

            /*
             * (non-Javadoc)
             * @see org.nrg.dcm.edit.Value#on(org.dcm4che2.data.DicomObject)
             */
            public String on(DicomObject o) throws ScriptEvaluationException {
                return evaluate(value.on(o), pattern.on(o), group);
            }

            /*
             * (non-Javadoc)
             * @see org.nrg.dcm.edit.Value#on(java.util.Map)
             */
            public String on(Map<Integer,String> m) throws ScriptEvaluationException {
                return evaluate(value.on(m), pattern.on(m), group);
            }

            /*
             * (non-Javadoc)
             * @see org.nrg.dcm.edit.Value#replaceVariable(org.nrg.dcm.edit.Variable)
             */
            public void replace(final Variable var) {
                value.replace(var);
                pattern.replace(var);
            }

            public VR vr() { return value.vr(); }

            public VR vrOn(final DicomObject o) throws ScriptEvaluationException {
                return value.vrOn(o);
            }

            public byte[] bytesOn(final DicomObject o) throws ScriptEvaluationException {
                final String s = on(o);
                return null == s ? null : s.getBytes();
            }
        };
    }

    private static String evaluate(final String value, final String pattern, final int group) {
        if (null == value) {
            return null;
        } else {
            final Matcher m = Pattern.compile(pattern).matcher(value);
            return m.matches() ? m.group(group) : null;
        }
    }
}
