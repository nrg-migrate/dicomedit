/**
 * Copyright (c) 2012-2013 Washington University School of Medicine
 */
package org.nrg.dcm.edit.fn;

import java.math.BigInteger;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.UUID;

import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.VR;
import org.nrg.dcm.edit.ScriptEvaluationException;
import org.nrg.dcm.edit.ScriptFunction;
import org.nrg.dcm.edit.Value;
import org.nrg.dcm.edit.Variable;

import com.fasterxml.uuid.Generators;

/**
 * Creates a one-way hash UID by first creating a Version 5 UUID (SHA-1 hash) from the provided string,
 * then converting that UUID to a UID.
 * Portions of this code are derived from dcm4che, an MPL 1.1-licensed open source project
 * hosted at http://sourceforge.net/projects/dcm4che.
 *
 * @author Kevin A. Archie <karchie@wustl.edu>
 * @author Gunter Zeilinger <gunterze@gmail.com> { original dcm4che implementation of UUID-to-UID conversion }
 *
 */
public final class HashUID implements ScriptFunction {
    public static final String name = "hashUID";

    /**
     * UID root for UUIDs (Universally Unique Identifiers) generated as per Rec. ITU-T X.667 | ISO/IEC 9834-8.
     * @see <a href="http://www.oid-info.com/get/2.25">OID repository {joint-iso-itu-t(2) uuid(25)}</a>
     */
    public static final String UUID_ROOT = "2.25";

    /* (non-Javadoc)
     * @see org.nrg.dcm.edit.ScriptFunction#apply(java.util.List)
     */
    public Value apply(final List<? extends Value> args) throws ScriptEvaluationException {
        if (args.isEmpty()) {
            throw new ScriptEvaluationException("usage: hashUID[string-to-hash {, optional algorithm-name} ]");
        }
        final Value sv = args.get(0);
        return new Value() {
            public Set<Variable> getVariables() {
                return sv.getVariables();
            }

            public SortedSet<Long> getTags() {
                return sv.getTags();
            }

            public String on(final DicomObject o) throws ScriptEvaluationException {
                try {
                    final String v = sv.on(o);
                    return null == v ? null : toUID(toUUID(v));
                } catch (ScriptEvaluationException e) {
                    throw e;
                } catch (Throwable t) {
                    throw new ScriptEvaluationException(t);
                }
            }

            public String on(Map<Integer,String> m) throws ScriptEvaluationException {
                try {
                    final String v = sv.on(m);
                    return null == v ? null : toUID(toUUID(v));
                } catch (ScriptEvaluationException e) {
                    throw e;
                } catch (Throwable t) {
                    throw new ScriptEvaluationException(t);
                }
            }
            
            public void replace(final Variable v) {
                sv.replace(v);
            }

            public VR vr() { return VR.UI; }

            public VR vrOn(final DicomObject _) { return VR.UI; }

            public byte[] bytesOn(final DicomObject o) throws ScriptEvaluationException {
                final String s = on(o);
                return null == s ? null : s.getBytes();
            }
        };
    }

    /* (non-Javadoc)
     * @see org.dcm4che2.util.UIDUtils#doCreateUID(java.util.String)
     * @param uuid
     * @return UID derived from the provided UUID
     */
    private static String toUID(final UUID uuid) {
        final byte[] b17 = new byte[17];
        fill(b17, 1, uuid.getMostSignificantBits());
        fill(b17, 9, uuid.getLeastSignificantBits());
        return new StringBuilder(64).append(UUID_ROOT).append('.')
        .append(new BigInteger(b17)).toString();
    }

    /* (non-Javadoc)
     * @see org.dcm4che2.util.UIDUtils#fill(byte[], int, long)
     */
    private static void fill(byte[] bb, int off, long val) {
        for (int i = off, shift = 56; shift >= 0; i++, shift -= 8)
            bb[i] = (byte) (val >>> shift);
    }

    /**
     * Generates a Version 5 UUID from the provided string
     * @param s source string
     * @return Version 5 UUID
     */
    private static UUID toUUID(final String s) {
        return Generators.nameBasedGenerator().generate(s.getBytes());
    }
}
