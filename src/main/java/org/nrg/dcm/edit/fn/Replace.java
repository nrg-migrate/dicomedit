/**
 * Copyright (c) 2010-2013 Washington University School of Medicine
 */
package org.nrg.dcm.edit.fn;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;

import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.VR;
import org.nrg.dcm.edit.ScriptEvaluationException;
import org.nrg.dcm.edit.ScriptFunction;
import org.nrg.dcm.edit.Value;
import org.nrg.dcm.edit.Variable;

import com.google.common.collect.Sets;

/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public final class Replace implements ScriptFunction {
    public static final String name = "replace";

    /* (non-Javadoc)
     * @see org.nrg.dcm.edit.ScriptFunction#apply(java.util.List)
     */
    public Value apply(final List<? extends Value> args) throws ScriptEvaluationException {
        final Value original, pre, post;
        try {
            original = args.get(0);
            pre = args.get(1);
            post = args.get(2);
        } catch (ArrayIndexOutOfBoundsException e) {
            throw new ScriptEvaluationException(name
                    + " takes three arguments [original, pre, post]", e);
        }

        return new Value() {
            /*
             * (non-Javadoc)
             * @see org.nrg.dcm.edit.Value#on(java.util.Map)
             */
            public String on(final Map<Integer,String> m) throws ScriptEvaluationException {
                return original.on(m).replace(pre.on(m), post.on(m));
            }

            /*
             * (non-Javadoc)
             * @see org.nrg.dcm.edit.Value#on(org.dcm4che2.data.DicomObject)
             */
            public String on(final DicomObject o) throws ScriptEvaluationException {
                return original.on(o).replace(pre.on(o), post.on(o));
            }

            /*
             * (non-Javadoc)
             * @see org.nrg.dcm.edit.Value#getVariables()
             */
            public Set<Variable> getVariables() {
                final Set<Variable> variables = Sets.newLinkedHashSet();
                variables.addAll(original.getVariables());
                variables.addAll(pre.getVariables());
                variables.addAll(post.getVariables());
                return variables;
            }

            /*
             * (non-Javadoc)
             * @see org.nrg.dcm.edit.Value#getTags()
             */
            public SortedSet<Long> getTags() {
                final SortedSet<Long> tags = Sets.newTreeSet();
                tags.addAll(original.getTags());
                tags.addAll(pre.getTags());
                tags.addAll(post.getTags());
                return tags;
            }
            
            /*
             * (non-Javadoc)
             * @see org.nrg.dcm.edit.Value#replaceVariable(org.nrg.dcm.edit.Variable)
             */
            public void replace(final Variable v) {
                original.replace(v);
                pre.replace(v);
                post.replace(v);
            }

            public VR vr() { return original.vr(); }

            public VR vrOn(final DicomObject o) throws ScriptEvaluationException {
                return original.vrOn(o);
            }

            public byte[] bytesOn(final DicomObject o) throws ScriptEvaluationException {
                final String s = on(o);
                return null == s ? null : s.getBytes();
            }
        };
    }
}
