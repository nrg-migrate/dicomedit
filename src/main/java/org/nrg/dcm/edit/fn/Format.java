/**
 * Copyright (c) 2010,2011 Washington University
 */
package org.nrg.dcm.edit.fn;

import java.util.List;

import org.nrg.dcm.edit.MessageFormatValue;
import org.nrg.dcm.edit.ScriptEvaluationException;
import org.nrg.dcm.edit.ScriptFunction;
import org.nrg.dcm.edit.Value;

/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public final class Format implements ScriptFunction {
    public static final String name = "format";

    /* (non-Javadoc)
     * @see org.nrg.dcm.edit.ScriptFunction#apply(java.util.List)
     */
    public Value apply(final List<? extends Value> args) throws ScriptEvaluationException {
        return new MessageFormatValue(args.get(0), args.subList(1, args.size()));
    }
}
