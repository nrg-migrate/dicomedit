/**
 * Copyright (c) 2010-2013 Washington University School of Medicine
 */
package org.nrg.dcm.edit.fn;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;

import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.VR;
import org.nrg.dcm.edit.IntegerValue;
import org.nrg.dcm.edit.ScriptEvaluationException;
import org.nrg.dcm.edit.ScriptFunction;
import org.nrg.dcm.edit.Value;
import org.nrg.dcm.edit.Variable;

/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public final class Substring implements ScriptFunction {
    public static final String name = "substring";

    /* (non-Javadoc)
     * @see org.nrg.dcm.edit.ScriptFunction#apply(java.util.List)
     */
    public Value apply(final List<? extends Value> args) throws ScriptEvaluationException {
        final Value s = args.get(0);
        final int start;
        final int end;
        try {
            start = ((IntegerValue)args.get(1)).getValue();
            end = ((IntegerValue)args.get(2)).getValue();
        } catch (ClassCastException e) {
            throw new ScriptEvaluationException("substring[ string, start{integer}, end{integer}]");
        }
        return new Value() {
            public Set<Variable> getVariables() { return s.getVariables(); }
            public SortedSet<Long> getTags() { return s.getTags(); }
            public String on(DicomObject o) throws ScriptEvaluationException {
                return s.on(o).substring(start, end);
            }
            public String on(Map<Integer,String> m) throws ScriptEvaluationException {
                return s.on(m).substring(start, end);
            }
            public void replace(final Variable v) {
                s.replace(v);
            }
            public VR vr() { return s.vr(); }
            public VR vrOn(final DicomObject o) throws ScriptEvaluationException {
                return s.vrOn(o);
            }
            public byte[] bytesOn(final DicomObject o) throws ScriptEvaluationException {
                final String s = on(o);
                return null == s ? null : s.getBytes();
            }
        };
    }
}
