package org.nrg.dcm.edit.fn;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;

import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.VR;
import org.nrg.dcm.edit.ScriptEvaluationException;
import org.nrg.dcm.edit.ScriptFunction;
import org.nrg.dcm.edit.Value;
import org.nrg.dcm.edit.Variable;
import org.nrg.util.HttpClient;
import org.nrg.util.IHttpClient;

import com.google.common.collect.ImmutableSortedSet;

public class GetURL implements ScriptFunction {
    public static final String name = "getURL";

    public Value apply(List<? extends Value> args) throws ScriptEvaluationException {
        final Value url = (Value) args.get(0);
        return new Value() {
            public Set<Variable> getVariables() { return Collections.emptySet(); }

            public SortedSet<Long> getTags() {
                return ImmutableSortedSet.of();
            }

            public String on(DicomObject o) throws ScriptEvaluationException {
                return request(url.on(o));
            }

            public String on(Map<Integer,String> m) throws ScriptEvaluationException {
                return request(url.on(m));
            }
            
            public void replace(final Variable v) {
                url.replace(v);
            }

            public VR vr() { return VR.UN; }

            public VR vrOn(final DicomObject _) { return VR.UN; }

            public byte[] bytesOn(final DicomObject o) throws ScriptEvaluationException {
                final String s = on(o);
                return null == s ? null : s.getBytes();
            }
        };
    }

    private String request(final String url) throws ScriptEvaluationException {
        try {
            final String response = getHttpClient().request(new URL(url));
            if (response == null) {
                throw new ScriptEvaluationException("Empty response from external webservice, " + url);
            }
            return response.trim();
        } catch (MalformedURLException e) {
            throw new ScriptEvaluationException("Improper URL, " + url);
        } catch (IOException e) {
            throw new ScriptEvaluationException("Error connecting to external webservice, " + url, e);
        }
    }

    // expose as protected so test case can override the implementation
    protected IHttpClient getHttpClient() {
        return new HttpClient();
    }
}
