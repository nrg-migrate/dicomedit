/**
 * Copyright (c) 2006-2012 Washington University
 */
package org.nrg.dcm.edit;

import com.google.common.base.Objects;


/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
final class ValueEqualsConstraint extends AbstractValueConstraint implements ConstraintMatch {
    ValueEqualsConstraint(final Value left, Value right) {
        super(left, right);
    }

    /*
     * (non-Javadoc)
     * @see org.nrg.dcm.edit.AbstractValueConstraint#matches(java.lang.String, java.lang.String)
     */
    protected boolean matches(final String left, final String right) {
        return Objects.equal(left, right);
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    public String toString() {
        return toString("equals");
    }
}
