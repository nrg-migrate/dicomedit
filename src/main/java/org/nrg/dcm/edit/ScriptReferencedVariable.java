/**
 * Copyright (c) 2009-2010 Washington University
 */
package org.nrg.dcm.edit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public final class ScriptReferencedVariable implements Variable {
    private final Logger logger = LoggerFactory.getLogger(ScriptReferencedVariable.class);
    private final String name;
    private String value = null;
    private Value initValue = null;
    private boolean isHidden = false;
    private String description = null;
    private String exportField = null;

    public ScriptReferencedVariable(final String name) {
        this.name = name;
    }

    /*
     * (non-Javadoc)
     * @see org.nrg.dcm.edit.Variable#getDescription()
     */
    public String getDescription() { return description; }

    /*
     * (non-Javadoc)
     * @see org.nrg.dcm.edit.Variable#getExportField()
     */
    public String getExportField() { return exportField; }

    /* (non-Javadoc)
     * @see org.nrg.dcm.edit.Variable#getInitialValue()
     */
    public Value getInitialValue() { return initValue; }

    /* (non-Javadoc)
     * @see org.nrg.dcm.edit.Variable#getName()
     */
    public String getName() { return name; }

    /* (non-Javadoc)
     * @see org.nrg.dcm.edit.Variable#getValue()
     */
    public String getValue() { return value; }

    /*
     * (non-Javadoc)
     * @see org.nrg.dcm.edit.Variable#isHidden()
     */
    public boolean isHidden() { return isHidden; }

    /*
     * (non-Javadoc)
     * @see org.nrg.dcm.edit.Variable#setDescription(java.lang.String)
     */
    public void setDescription(final String description) {
        this.description = description;
    }

    /*
     * (non-Javadoc)
     * @see org.nrg.dcm.edit.Variable#setExportField(java.lang.String)
     */
    public void setExportField(final String exportField) {
        this.exportField = exportField;
    }

    /* (non-Javadoc)
     * @see org.nrg.dcm.edit.Variable#setInitialValue(org.nrg.dcm.edit.Value)
     */
    public void setInitialValue(final Value value)
    throws MultipleInitializationException {
        if (null == initValue) {
            initValue = value;
        } else {
            throw new MultipleInitializationException(this, value);
        }

    }

    /*
     * (non-Javadoc)
     * @see org.nrg.dcm.edit.Variable#setIsHidden(boolean)
     */
    public void setIsHidden(final boolean isHidden) {
        this.isHidden = isHidden;
    }

    /* (non-Javadoc)
     * @see org.nrg.dcm.edit.Variable#setValue(java.lang.String)
     */
    public void setValue(final String value) {
        logger.trace("set {} to {}", this, value);
        this.value = value;
    }


    /*
     * (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    public boolean equals(final Object o) {
        return o instanceof Variable && name.equals(((Variable)o).getName());
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    public int hashCode() { return name.hashCode(); }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    public String toString() {
        return new StringBuffer(super.toString()).append(" (").append(name).append(")").toString();
    }
}
