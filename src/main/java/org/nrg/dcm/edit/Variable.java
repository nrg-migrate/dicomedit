/**
 * Copyright (c) 2009,2010 Washington University
 */
package org.nrg.dcm.edit;


/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public interface Variable {
    /**
     * Gets the human-readable description of this variable.
     * @return description, or null if not set
     */
	String getDescription();
	
	/**
	 * Gets the label that will be used for exporting this variable.
	 * @return export field label, or null if not set
	 */
	String getExportField();
	
	/**
	 * Gets the (brief) name of this variable.
	 * @return
	 */
	
    String getName();
    
    /**
     * Gets the initial Value assigned to this variable.
     * @return initialization Value, or null if not set
     */
    Value getInitialValue();
    
    /**
     * Gets the concrete value for this variable.
     * @return value
     */
    String getValue();
    
    /**
     * Should this variable be hidden in a user interface?
     * @return true if this variable should be hidden from users
     */
    boolean isHidden();
    
    /**
     * Sets the human-readable description of this variable.
     * @param description
     */
    void setDescription(String description);
    
    /**
     * Sets the label to be used for exporting this variable.
     * @param field
     */
    void setExportField(String field);
    
    /**
     * Sets an initialization Value for this variable
     * @param value
     * @throws MultipleInitializationException if an initial value has already been assigned
     */
    void setInitialValue(Value value) throws MultipleInitializationException;
    
    /**
     * Specifies whether this variable should be hidden in a user interface.
     * @param isHidden
     */
    void setIsHidden(boolean isHidden);
    
    /**
     * Sets the concrete value of this variable.
     * @param value
     */
    void setValue(String value);
 }
