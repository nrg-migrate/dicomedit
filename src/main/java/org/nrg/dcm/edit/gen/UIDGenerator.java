/**
 * Copyright (c) 2010,2011 Washington University
 */
package org.nrg.dcm.edit.gen;

import org.dcm4che2.util.UIDUtils;
import org.nrg.dcm.edit.ValueGenerator;

/**
 * Generates fresh UIDs from randomly generated UUIDs
 * @author Kevin A. Archie <karchie@wustl.edu>
 */
public final class UIDGenerator implements ValueGenerator {
    public final static String name = "UID";

    /*
     * (non-Javadoc)
     * @see org.nrg.dcm.edit.ValueGenerator#valueFor(java.lang.String, java.lang.Iterable)
     */
    public String valueFor(final String old, final Iterable<String> params) {
        return UIDUtils.createUID();
    }
}
