/**
 * Copyright (c) 2006-2011 Washington University
 */
package org.nrg.dcm.edit;

import java.util.SortedSet;

import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.VR;
import org.dcm4che2.util.StringUtils;
import org.dcm4che2.util.TagUtils;

import com.google.common.collect.ImmutableSortedSet;

/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
class SimpleConstraintMatch implements ConstraintMatch {
    private final int tag;
    private final String pattern;

    SimpleConstraintMatch(final int tag, final String pattern) {
        this.tag = tag;
        this.pattern = pattern;
    }

    SimpleConstraintMatch(final Integer tag, final String pattern) {
        this(tag.intValue(), pattern);
    }

    public SortedSet<Long> getTags() {
        return ImmutableSortedSet.of(0xffffffffL & tag);
    }

    final String getPattern() { return pattern; }

    boolean matches(final String value) {
        return value.equals(pattern);
    }

    public boolean matches(final DicomObject o) {
        if (o.contains(tag)) {
            final VR vr = o.vrOf(tag);
            final String value;
            if (VR.SQ == vr) { 
                throw new RuntimeException("can't use SQ type attribute for constraint");
            } else if (VR.UN == vr) {
                value = o.getString(tag);
            } else {
                value = StringUtils.join(o.getStrings(tag), '\\');
            }
            return matches(value);
        } else
            return false;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    public String toString() {
        return "Constraint: " + TagUtils.toString(tag) + " matches " + pattern;
    }
}
