/**
 * Copyright (c) 2009,2012 Washington University
 */
package org.nrg.dcm.edit;

/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
final class ValueRegexConstraint extends AbstractValueConstraint {
    public ValueRegexConstraint(final Value value, final Value pattern) {
        super(value, pattern);
    }


    /*
     * (non-Javadoc)
     * @see org.nrg.dcm.edit.ValueConstraintMatch#matches(java.lang.String, java.lang.String)
     */
    protected boolean matches(final String value, final String pattern)
    throws ScriptEvaluationException {
        if (null == pattern) {
            throw new ScriptEvaluationException("null pattern not allowed");
        } else if (null == value) {
            return false;
        } else {
            return value.matches(pattern);
        }
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    public String toString() {
        return toString("matches");
    }
}
