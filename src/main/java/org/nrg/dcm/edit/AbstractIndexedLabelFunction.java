/**
 * Copyright (c) 2010-2013 Washington University School of Medicine
 */
package org.nrg.dcm.edit;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;

import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.VR;

/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public abstract class AbstractIndexedLabelFunction implements ScriptFunction {
    protected abstract boolean isAvailable(String label) throws ScriptEvaluationException;
    
    private final Value getFormat(final List<? extends Value> values)
    throws ScriptEvaluationException {
	try {
	    return values.get(0);
	} catch (IndexOutOfBoundsException e) {
	    try {
		throw new ScriptEvaluationException(this.getClass().getField("name").get(null)
						    + " requires format argument");
	    } catch (IllegalArgumentException e1) {
		throw new RuntimeException(e1);
	    } catch (SecurityException e1) {
		throw new RuntimeException(e1);
	    } catch (IllegalAccessException e1) {
		throw new RuntimeException(e1);
	    } catch (NoSuchFieldException e1) {
		throw new RuntimeException(e1);
	    }
	}
    }
    
    /* (non-Javadoc)
     * @see org.nrg.dcm.edit.ScriptFunction#apply(java.util.List)
     */
    public Value apply(final List<? extends Value> args) throws ScriptEvaluationException {
	final Value format = getFormat(args);
	return new Value() {
	    private NumberFormat buildFormatter(final int len) {
		final StringBuilder sb = new StringBuilder();
		for (int i = 0; i < len; i++) {
		    sb.append("0");
		}
		return new DecimalFormat(sb.toString());
	    }
	    
	    private String valueFor(final String format) throws ScriptEvaluationException {
		final int offset = format.indexOf('#');
		if (-1 == offset) {
		    return "";
		}
		int end = offset;
		while (end < format.length() && '#' == format.charAt(end)) {
		    end++;
		}
		final NumberFormat nf = buildFormatter(end - offset);
		
		final StringBuffer sb = new StringBuffer(format);
		
		for (int i = 0; true; i++) {
		    final String label = sb.replace(offset, end, nf.format(i)).toString();
		    if (isAvailable(label)) {
			return label;
		    }
		}				
	    }
	    
	    public String on(final Map<Integer,String> m) throws ScriptEvaluationException {
		return valueFor(format.on(m));
	    }
	    
	    public String on(final DicomObject o) throws ScriptEvaluationException {
		return valueFor(format.on(o));
	    }
	    
	    public void replace(final Variable v) {
	        format.replace(v);
	    }
	    
	    public Set<Variable> getVariables() { return format.getVariables(); }
	    
	    public SortedSet<Long> getTags() { return format.getTags(); }

        public VR vr() { return VR.UN; }

        public VR vrOn(final DicomObject _) { return VR.UN; }

        public byte[] bytesOn(final DicomObject o) throws ScriptEvaluationException {
            final String s = on(o);
            return null == s ? null : s.getBytes();
        }
	};
    }
}
