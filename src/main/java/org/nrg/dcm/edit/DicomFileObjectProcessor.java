/**
 * Copyright 2009 Washington University
 */
package org.nrg.dcm.edit;

import java.io.File;
import java.io.IOException;

import org.dcm4che2.data.DicomObject;

/**
 * @author Kevin A. Archie <karchie@npg.wustl.edu>
 *
 */
public interface DicomFileObjectProcessor {
    Object process(File file, DicomObject o) throws IOException;
}
