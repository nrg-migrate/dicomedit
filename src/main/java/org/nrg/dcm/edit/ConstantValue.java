/**
 * Copyright (c) 2009-2013 Washington University School of Medicine
 */
package org.nrg.dcm.edit;

import java.util.Map;
import java.util.Set;
import java.util.SortedSet;

import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.VR;

import com.google.common.collect.ImmutableSortedSet;

/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public final class ConstantValue implements Value {
    private final String value;
    private final VR vr;

    public ConstantValue(final String value, final VR vr) {
        this.value = value;
        this.vr = vr;
    }
    
    public ConstantValue(final String value) {
        this(value, VR.UN);
    }

    /*
     * (non-Javadoc)
     * @see org.nrg.dcm.edit.Value#getTags()
     */
    public SortedSet<Long> getTags() { return ImmutableSortedSet.of(); }

    /*
     * (non-Javadoc)
     * @see org.nrg.dcm.edit.Value#getVariables()
     */
    public Set<Variable> getVariables() { return ImmutableSortedSet.of(); }

    /* (non-Javadoc)
     * @see org.nrg.dcm.edit.Value#on(org.dcm4che2.data.DicomObject)
     */
    public String on(final DicomObject o) {
        return value;
    }

    /* (non-Javadoc)
     * @see org.nrg.dcm.edit.Value#on(java.util.Map)
     */
    public String on(final Map<Integer,String> m) {
        return value;
    }

    /*
     * (non-Javadoc)
     * @see org.nrg.dcm.edit.Value#replaceVariable(org.nrg.dcm.edit.Variable)
     */
    public void replace(final Variable _) {}
    
    
    /*
     * (non-Javadoc)
     * @see org.nrg.dcm.edit.Value#vr()
     */
    public VR vr() { return vr; }
    
    /*
     * (non-Javadoc)
     * @see org.nrg.dcm.edit.Value#vrOn(org.dcm4che2.data.DicomObject)
     */
    public VR vrOn(final DicomObject _) { return vr; }
    /*
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    public String toString() {
        final StringBuilder sb = new StringBuilder(super.toString());
        sb.append(" (").append(value).append(")");
        return sb.toString();
    }

    /* (non-Javadoc)
     * @see org.nrg.dcm.edit.Value#bytesOn(org.dcm4che2.data.DicomObject)
     */
    public byte[] bytesOn(DicomObject o) {
        final String s = on(o);
        return null == s ? null : s.getBytes();
    }
}
