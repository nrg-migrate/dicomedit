/**
 * Copyright (c) 2006-2011 Washington University
 */
package org.nrg.dcm.edit;


/**
 * Represents an action to be performed on specific DICOM attributes
 * @author Kevin A. Archie <karchie@wustl.edu>
 */
public interface Action {
    /**
     * Performs the associated action.
     */
    void apply() throws ScriptEvaluationException;
}
