/**
 * Copyright (c) 2009-2013 Washington University School of Medicine
 */
package org.nrg.dcm.edit;

import java.util.Map;
import java.util.Set;
import java.util.SortedSet;

import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.VR;

/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public final class SubstringValue implements Value {
    private Value base;
    private final int beginIndex, endIndex;

    public SubstringValue(final Value base, final int beginIndex, final int endIndex) {
        this.base = base;
        this.beginIndex = beginIndex;
        this.endIndex = endIndex;
    }

    public SubstringValue(final Value base, final Integer beginIndex, final Integer endIndex) {
        this(base, beginIndex.intValue(), endIndex.intValue());
    }


    /* (non-Javadoc)
     * @see org.nrg.dcm.edit.Value#getTags()
     */
    public SortedSet<Long> getTags() { return base.getTags(); }

    /*
     * (non-Javadoc)
     * @see org.nrg.dcm.edit.Value#getVariables()
     */
    public Set<Variable> getVariables() { return base.getVariables(); }


    private String substring(final String s) throws ScriptEvaluationException {
        if (null == s) {
            return null;
        }
        try {
            return s.substring(beginIndex, endIndex);
        } catch (IndexOutOfBoundsException e) {
            throw new ScriptEvaluationException("index out of bounds", e);
        }
    }

    /* (non-Javadoc)
     * @see org.nrg.dcm.edit.Value#on(org.dcm4che2.data.DicomObject)
     */
    public String on(final DicomObject o) throws ScriptEvaluationException {
        return substring(base.on(o));
    }

    /* (non-Javadoc)
     * @see org.nrg.dcm.edit.Value#on(java.util.Map)
     */
    public String on(final Map<Integer,String> m) throws ScriptEvaluationException {
        return substring(base.on(m));
    }
    
    /*
     * (non-Javadoc)
     * @see org.nrg.dcm.edit.Value#replaceVariable(org.nrg.dcm.edit.Variable)
     */
    public void replace(final Variable v) {
        base.replace(v);
    }

    /* (non-Javadoc)
     * @see org.nrg.dcm.edit.Value#vr()
     */
    public VR vr() {
        return base.vr();
    }

    /* (non-Javadoc)
     * @see org.nrg.dcm.edit.Value#vrOn(org.dcm4che2.data.DicomObject)
     */
    public VR vrOn(DicomObject o) throws ScriptEvaluationException {
        return base.vrOn(o);
    }

    /* (non-Javadoc)
     * @see org.nrg.dcm.edit.Value#bytesOn(org.dcm4che2.data.DicomObject)
     */
    public byte[] bytesOn(final DicomObject o) throws ScriptEvaluationException {
        final String s = on(o);
        return null == s ? null : s.getBytes();
    }
}
