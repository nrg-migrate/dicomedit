/**
 * Copyright (c) 2006-2009 Washington University
 */
package org.nrg.dcm.edit;

import org.dcm4che2.data.VR;
import org.dcm4che2.util.TagUtils;

/**
 * @author Kevin A. Archie <karchie@npg.wustl.edu>
 *
 */
public class AttributeVRMismatchException extends AttributeException {
    private static final long serialVersionUID = 1;

    AttributeVRMismatchException(int tag, VR vr) {
	super("Cannot use DICOM attribute " + TagUtils.toString(tag)
		+ " -- uninterpretable type " + vr);
    }
}
