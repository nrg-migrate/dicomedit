/**
 * Copyright (c) 2014 Washington University School of Medicine
 */
package org.nrg.dcm.edit;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.dcm4che2.data.DicomElement;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.util.TagUtils;

/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public class ExactTag implements ElementMatcher {
    private final int[] tagpath;

    public ExactTag(final int...tag) {
        this.tagpath = tag;
    }
    
    private static int[] toIntArray(final Collection<Integer> xs) {
        int[] a = new int[xs.size()];
        int i = 0;
        for (int x : xs) {
            a[i++] = x;
        }
        return a;
    }
    
    public ExactTag(final List<Integer> tagpath) {
        this(toIntArray(tagpath));
    }

    /*
     * (non-Javadoc)
     * @see com.google.common.base.Function#apply(java.lang.Object)
     */
    public Iterable<int[]> apply(final DicomObject o) {
        if( tagpath.length == 1) {
            // always return the tag path for single-element tagpaths so the tag is created if it doesn't exist.
            return Collections.singletonList(tagpath);
        }
        else {
            // otherwise, only return tagpath if it already exists.
            final DicomElement e = o.get(tagpath);
            if (null == e) {
                return Collections.emptyList();
            } else {
                return Collections.singletonList(tagpath);
            }
        }
    }

    /*
     * (non-Javadoc)
     * @see com.google.common.base.Predicate#apply(java.lang.Object)
     */
    public boolean apply(final Integer tag) {
        return null != tag && this.tagpath.length == 1 && this.tagpath[0] == tag;
    }

    /*
     * (non-Javadoc)
     * @see org.nrg.dcm.edit.ElementMatcher#getTopTag()
     */
    public long getTopTag() {
        return tagpath[0];
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append(TagUtils.toString(tagpath[0]));
        for (int i = 1; i < tagpath.length; i++) {
            sb.append('[').append(tagpath[i]).append(']');
            sb.append(TagUtils.toString(tagpath[0]));
        }
        return sb.toString();
    }
}
