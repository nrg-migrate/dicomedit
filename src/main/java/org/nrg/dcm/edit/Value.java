/**
 * Copyright (c) 2009-2011 Washington University
 */
package org.nrg.dcm.edit;

import java.util.Map;
import java.util.Set;
import java.util.SortedSet;

import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.VR;

/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public interface Value {
    byte[] bytesOn(DicomObject o) throws ScriptEvaluationException;
    SortedSet<Long> getTags();
    Set<Variable> getVariables();
    String on(DicomObject o) throws ScriptEvaluationException;
    String on(Map<Integer,String> m) throws ScriptEvaluationException;
    void replace(Variable v);
    VR vr();
    VR vrOn(DicomObject o) throws ScriptEvaluationException;
}
