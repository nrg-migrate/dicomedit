/**
 * Copyright 2009-2013 Washington University School of Medicine
 */
package org.nrg.dcm.edit;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;

import org.antlr.runtime.ANTLRInputStream;
import org.antlr.runtime.CommonTokenStream;
import org.antlr.runtime.RecognitionException;
import org.antlr.runtime.tree.CommonTree;
import org.antlr.runtime.tree.CommonTreeNodeStream;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.io.DicomOutputStream;
import org.nrg.dcm.DicomUtils;
import org.nrg.dcm.edit.fn.Format;
import org.nrg.dcm.edit.fn.GetURL;
import org.nrg.dcm.edit.fn.HashUID;
import org.nrg.dcm.edit.fn.Lowercase;
import org.nrg.dcm.edit.fn.Match;
import org.nrg.dcm.edit.fn.Replace;
import org.nrg.dcm.edit.fn.Substring;
import org.nrg.dcm.edit.fn.Uppercase;
import org.nrg.dcm.edit.fn.UrlEncode;
import org.nrg.dcm.edit.gen.UIDGenerator;
import org.nrg.io.PresuffixFileMapper;
import org.nrg.framework.utilities.GraphUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Function;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;


/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public final class ScriptApplicator {
    private final Logger logger = LoggerFactory.getLogger(ScriptApplicator.class);
    private final List<Statement> statements;
    private final EditDCMTreeParser astParser;

    public ScriptApplicator(final InputStream script,
            final Map<String,ScriptFunction> functions,
            final Map<String,ValueGenerator> generators)
    throws IOException,ScriptEvaluationException {
        if (null == script) {
            statements = Collections.emptyList();
            astParser = null;
        } else {
            try {
                final EditDCMLexer lexer = new EditDCMLexer(new ANTLRInputStream(script));
                final EditDCMParser parser = new EditDCMParser(new CommonTokenStream(lexer));
                final EditDCMParser.script_return sr = parser.script();  // start rule method

                logger.trace("parsing to AST");
                final CommonTree ast = (CommonTree) sr.getTree();
                if (null == ast) {
                    logger.trace("unparseable");
                    astParser = null;
                    statements = Lists.newArrayList();
                } else {
                    astParser = new EditDCMTreeParser(new CommonTreeNodeStream(ast));
                    astParser.setFunction(Substring.name, new Substring());
                    astParser.setFunction(Format.name, new Format());
                    astParser.setFunction(Lowercase.name, new Lowercase());
                    astParser.setFunction(Uppercase.name, new Uppercase());
                    astParser.setFunction(GetURL.name, new GetURL());
                    astParser.setFunction(Replace.name, new Replace());
                    astParser.setFunction(Match.name, new Match());
                    astParser.setFunction(UrlEncode.name, new UrlEncode());
                    astParser.setFunction(HashUID.name, new HashUID());
                    for (final Map.Entry<String,ScriptFunction> me : functions.entrySet()) {
                        logger.trace("adding function {}", me);
                        astParser.setFunction(me.getKey(), me.getValue());
                    }

                    for (final Map.Entry<String,ValueGenerator> me : generators.entrySet()) {
                        logger.trace("adding generator {}", me);
                        astParser.setGenerator(me.getKey(), me.getValue());
                    }
                    if (!generators.containsKey(UIDGenerator.name)) {
                        logger.trace("adding generator {}", UIDGenerator.name);
                        astParser.setGenerator(UIDGenerator.name, new UIDGenerator());
                    }
                    logger.trace("ready to parse");
                    statements = astParser.script();
                    logger.trace("{}", statements);
                }
            } catch (RecognitionException e) {
                throw new ScriptEvaluationException("error parsing script", e);
            }
        }
    }

    public ScriptApplicator(final InputStream script, final Map<String,ScriptFunction> functions)
    throws IOException,ScriptEvaluationException {
        this(script, functions, new HashMap<String,ValueGenerator>());
    }

    public ScriptApplicator(final InputStream script)
    throws IOException,ScriptEvaluationException {
        this(script, new HashMap<String,ScriptFunction>(), new HashMap<String,ValueGenerator>());
    }

    public ScriptApplicator() {
        statements = Collections.emptyList();
        astParser = null;
    }

    public int getTopTag() {
        final SortedSet<Long> tags = Sets.newTreeSet();
        tags.add(0L);
        for (final Statement s : statements) {
            tags.add(s.getTopTag());
        }
        return tags.last().intValue();
    }
    
    public Variable getVariable(final String label) {
        return null == astParser ? null : astParser.getVariable(label);
    }

    public Map<String,Variable> getVariables() {
        return null == astParser ? Collections.<String,Variable>emptyMap() : astParser.getVariables();
    }

    public List<Variable> getSortedVariables(final Collection<String> excluding) {
        final Map<Variable,Collection<Variable>> graph = Maps.newLinkedHashMap();
        for (final Variable v : getVariables().values()) {
            if (!excluding.contains(v.getName())) {
                final Value iv = v.getInitialValue();
                final Collection<Variable> dependencies = Lists.newArrayList();
                if (null != iv) {
                    for (final Variable dv : iv.getVariables()) {
                        if (!excluding.contains(dv.getName())) {
                            dependencies.add(dv);
                        }
                    }
                }
                graph.put(v, dependencies);
            }
        }
        
        // Don't let externally- but not internally-defined variables get
        // in the way of our dependency resolution.
        for (final Collection<Variable> dependencies : graph.values()) {
            for (final Iterator<Variable> i = dependencies.iterator(); i.hasNext(); ) {
                if (!graph.containsKey(i.next())) {
                    i.remove();
                }
            }
        }
        
        logger.trace("sorting variable dependencies: {}", graph);
        return GraphUtils.topologicalSort(graph);
    }


    public List<Variable> getSortedVariables(final String[] excluding) {
        return getSortedVariables(Arrays.asList(excluding));
    }

    public List<Variable> getSortedVariables() {
        return this.getSortedVariables(Collections.<String>emptyList());
    }

    public List<Statement> getStatements() {
        return statements;
    }

    public DicomObject apply(final File f, final DicomObject o)
    throws AttributeException,ScriptEvaluationException {
        final List<Action> actions = Statement.getActions(statements, f, o);
        // In order to ensure the semantics of multiple matching operations
        // (i.e., that the first match applies), we apply the operations
        // in reverse order.
        Collections.reverse(actions);
        for (final Action action : actions) {
            action.apply();
        }
        return o;
    }


    public DicomObject apply(final File f)
    throws IOException,AttributeException,ScriptEvaluationException {
        return apply(f, DicomUtils.read(f));
    }

    public void setGenerator(final String label, final ValueGenerator generator) {
        if (null != astParser) {
            astParser.setGenerator(label, generator);
        }
    }

    public void setFunction(final String label, final ScriptFunction function) {
        if (null != astParser) {
            astParser.setFunction(label, function);
        }
    }

    /**
     * Unify the provided variable with matching variables in this environment.
     * If a similarly-named variable already exists in this environment, replace
     * that one with the provided v and return v. Otherwise, do not add v to the
     * environment and return null.
     * If v has no initial value set, but the previous variable does, v's initial
     * value is set to that of the previous variable.
     * @param v Variable that should replace any similarly-named variables in
     *           this parser's environment.
     * @return v if a similarly-named variable already existed here, null otherwise
     */
    public Variable unify(final Variable v) {
        final Variable vn = astParser.unify(v);
        for (final Statement s : statements) {
            final Operation o = s.getOperation();
            if (null != o) {
                o.replace(v);
            }
        }
        return vn;
    }

    public static void main(final String args[]) throws Exception {
        final InputStream in;
        if (0 == args.length || "-".equals(args[0])) {
            in = System.in;
        } else {
            in = new FileInputStream(args[0]);
        }
        final ScriptApplicator applicator;
        try {
            applicator = new ScriptApplicator(in);
        } finally {
            in.close();
        }

        final Function<File,File> mapper = new PresuffixFileMapper("-mod");

        for (int i = 1; i < args.length; i++) {
            final File f = new File(args[i]);
            final DicomOutputStream out = new DicomOutputStream(new FileOutputStream(mapper.apply(f)));
            try {
                out.writeDicomFile(applicator.apply(f));
            } finally {
                out.close();
            }
        }
    }
}
