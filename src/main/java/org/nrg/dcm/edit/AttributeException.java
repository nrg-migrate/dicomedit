/*
 * Copyright (c) 2009,2011 Washington University
 */
package org.nrg.dcm.edit;

/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public abstract class AttributeException extends Exception {
    private static final long serialVersionUID = -5568999633339020555L;

    public AttributeException(final String s) { super(s); }
}
