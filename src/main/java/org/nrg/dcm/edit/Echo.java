/**
 * Copyright (c) 2006-2013 Washington University
 */
package org.nrg.dcm.edit;

import java.util.Map;
import java.util.SortedSet;

import org.dcm4che2.data.DicomObject;


/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 */
public final class Echo implements Operation {
    private final Value v;

    public Echo(final String message) {
        this(new ConstantValue(message));
    }

    public Echo(final Value v) {
        this.v = v;
    }

    /*
     * (non-Javadoc)
     * @see org.nrg.dcm.edit.Operation#affects(int)
     */
    public boolean affects(final int _) { return false; }

    /* (non-Javadoc)
     * @see org.nrg.dcm.edit.Operation#makeAction(org.dcm4che2.data.DicomObject)
     */
    public Action makeAction(final DicomObject o) {
        String m;
        try {
            m = v.on(o);
        } catch (ScriptEvaluationException e) {
            m = "[uninterpretable: " + v + "]";
        }
        final String message = m;
        return new Action() {
            public void apply() { System.out.print(message); }
            public String toString() { return getName() + ": " + message; }
        };
    }

    /*
     * (non-Javadoc)
     * @see org.nrg.dcm.edit.Operation#getTopTag()
     */
    public long getTopTag() {
        final SortedSet<Long> tags = v.getTags();
        return tags.isEmpty() ? 0 : tags.last();
    }

    /*
     * (non-Javadoc)
     * @see org.nrg.dcm.edit.Operation#apply(java.util.Map)
     */
    public String apply(final Map<Integer,String> vals) {
        String m;
        try {
            m = v.on(vals);
        } catch (ScriptEvaluationException e) {
            m = "[uninterpretable: " + v + "]";
        }
        System.out.print(m);
        return null;
    }

    /*
     * (non-Javadoc)
     * @see org.nrg.dcm.edit.Operation#replace(org.nrg.dcm.edit.Variable)
     */
    public void replace(final Variable var) {
        v.replace(var);
    }
    
    /*
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    public String toString() { return v.toString(); }

    /*
     * (non-Javadoc)
     * @see org.nrg.dcm.edit.Operation#getName()
     */
    public String getName() { return "Echo"; }
}
