/**
 * Copyright (c) 2009 Washington University
 */
package org.nrg.dcm.edit;


/**
 * @author Kevin A. Archie <karchie@npg.wustl.edu>
 *
 */
public class MultipleInitializationException extends Exception {
    private static final long serialVersionUID = 1L;
    
    public MultipleInitializationException(final Variable v, final Value value) {
	super("Attempted to reinitialize variable " + v + " -> " + value);
    }
}
