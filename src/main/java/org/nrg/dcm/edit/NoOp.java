/*
 * Copyright (c) 2006-2013 Washington University School of Medicine
 */
package org.nrg.dcm.edit;

import java.util.Map;

import org.dcm4che2.data.DicomObject;

/**
 * Null operation
 * @author Kevin A. Archie <karchie@wustl.edu>
 */
public class NoOp extends AbstractOperation {
    private static final String id = "NoOp";

    public NoOp() { super(id); }

    public Action makeAction(DicomObject o) throws AttributeException {
        return new Action() {
            public void apply() {}
            public String toString() { return id; }
        };
    }

    /*
     * (non-Javadoc)
     * @see org.nrg.dcm.edit.Operation#affects(int)
     */
    public boolean affects(final int _) { return false; }
    
    /*
     * (non-Javadoc)
     * @see org.nrg.dcm.edit.Operation#apply(java.util.Map)
     */
    public String apply(Map<Integer,String> _) {
        return null;
    }

    /*
     * all NoOp objects are equivalent
     * (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    public boolean equals(Object o) { return o instanceof NoOp; }

    /*
     * (non-Javadoc)
     * @see org.nrg.dcm.edit.Operation#getTopTag()
     */
    public long getTopTag() { return 0; }

    /*
     * (non-Javadoc)
     * @see org.nrg.dcm.edit.Operation#replace(org.nrg.dcm.edit.Variable)
     */
    public void replace(final Variable _) {}
    
    /*
     * (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    public int hashCode() { return 41; }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    public String toString() { return id; }
}
