/**
 * Copyright (c) 2012-2014 Washington University School of Medicine
 */
package org.nrg.dcm.edit;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.dcm4che2.data.DicomElement;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.util.TagUtils;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;

/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public class TagPattern implements ElementMatcher {
    public static final Character ANY_DIGIT = Character.valueOf('X');
    public static final Character EVEN_DIGIT = Character.valueOf('@');
    public static final Character ODD_DIGIT = Character.valueOf('#');
    public static final ImmutableSet<Character> HEX_DIGITS = ImmutableSet.of('0','1','2','3','4','5','6','7','8','9','a','A','b','B','c','C','d','D','e','E','f','F');

    private static final long MASK_ALL = 0xffffffffL, MAX_EVEN = 0xeeeeeeeeL, MAX_ODD = 0xffffffffL;
    private static final Pattern tagPattern = Pattern.compile("\\(([X@#0-9A-Fa-f]{4})\\,([X@#0-9A-Fa-f]{4})\\)");
    private static final Pattern indexPattern = Pattern.compile("\\[([X0-9]|([1-9][0-9]+))\\]");

    private final String textpath;

    private final long[] exactMask, anyMask, evenMask, oddMask, exactPart;

    private TagPattern(final String spec, final List<String> specs) throws ScriptEvaluationException {
        this.textpath = spec;
        final int length = specs.size();
        exactMask = new long[length];
        anyMask = new long[length];
        evenMask = new long[length];
        oddMask = new long[length];
        exactPart = new long[length];
        for (int i = 0; i < length; i++) {
            final String s = specs.get(i);
            if (0 == i%2) { // tag pattern
                long exactm = 0L, anym = 0L, evenm = 0L, oddm = 0L, exactp = 0L;
                for (int ci = 0; ci < s.length(); ci++) {
                    exactm <<= 4;
                    anym <<= 4;
                    evenm <<= 4;
                    oddm <<= 4;
                    exactp <<= 4;
                    final Character c = s.charAt(ci);
                    if (HEX_DIGITS.contains(c)) {
                        exactm |= 0xf;
                        exactp |= Integer.parseInt(String.valueOf(c), 16);
                    } else if (ANY_DIGIT.equals(c)) {
                        anym |= 0xf;
                    } else if (ODD_DIGIT.equals(c)) {
                        oddm |= 0xf;
                    } else if (EVEN_DIGIT.equals(c)) {
                        evenm |= 0xf;
                    } else {
                        throw new ScriptEvaluationException("unrecognized tag digit '" + c + "' in \"" + s + "\"");
                    }
                }
                assert (exactm | anym | evenm | oddm) == MASK_ALL;
                assert (exactm & anym) == 0;
                assert (exactm & evenm) == 0;
                assert (exactm & oddm) == 0;
                assert (anym & evenm) == 0;
                assert (anym & oddm) == 0;
                assert (evenm & oddm) == 0;
                exactMask[i] = exactm;
                anyMask[i] = anym;
                evenMask[i] = evenm;
                oddMask[i] = oddm;
                this.exactPart[i] = exactp;
            } else if ("X".equals(s)) { // wildcard index
                anyMask[i] = MASK_ALL;
            } else { // must be numeric index
                try {
                    exactPart[i] = Long.parseLong(specs.get(i));
                } catch (NumberFormatException e) {
                    throw new ScriptEvaluationException("expected SQ index, found " + s, e);
                }
            }
        }
    }
    
    /**
     * Builds a tag matcher from a tag specification of the form gggg,eeee
     * For each of the four digits, a hex digit 0-9a-fA-F requires a value match
     * X indicates any digit is acceptable
     * @ indicates any even digit is acceptable [02468ace]
     * # indicates any odd digit is acceptable [13579bdf]
     * @param spec tag specification
     */
    public TagPattern(final List<String> components) throws ScriptEvaluationException {
        this(reconstructSpec(components), normalizeComponents(components));
    }
    
    /**
     * Builds a tag matcher from a tag specification of the form gggg,eeee
     * For each of the four digits, a hex digit 0-9a-fA-F requires a value match
     * X indicates any digit is acceptable
     * @ indicates any even digit is acceptable [02468ace]
     * # indicates any odd digit is acceptable [13579bdf]
     * @param spec tag specification
     */
    public TagPattern(final String spec) throws ScriptEvaluationException {
        this(spec, tokenizeSpec(spec));
    }
    
    /**
     * The tree parser produces tagpaths that look like tag::int (index::string tag::int)*
     * Convert that into the (gggg,eeee) (index::string (gggg,eeee))*
     * used by the TagPattern constructors
     * @param components tagpath as tag::int (index::string tag::int)*
     * @return tagpath as tag::string ('[' index::string ']' taghex::string)*
     */
    private static List<String> asStringComponents(final Iterable<?> components) {
        final ImmutableList.Builder<String> builder = ImmutableList.builder();
        int i = 0;
        for (final Object component : components) {
            if (0 == i % 2) {
                builder.add(TagUtils.toString((Integer)component));
            } else {
                builder.add('[' + (String)component + ']');
            }
            i++;
        }
        return builder.build();
    }
    
    /**
     * Returns an ElementMatcher based on the tree parser tagpath
     * @param components tagpath as tag::int (index::string tag::int)*
     * @return ExactTag if tagpath is exact; TagPattern otherwise
     * @throws ScriptEvaluationException
     */
    public static ElementMatcher fromTagPathComponents(final Iterable<?> components)
    throws ScriptEvaluationException {
        final ImmutableList.Builder<Integer> builder = ImmutableList.builder();
        int i = 0;
        for (final Object component : components) {
            if (0 == i % 2) {
                builder.add((Integer)component);
            } else if ("X".equals(component)) {
                return new TagPattern(asStringComponents(components));
            } else {
                builder.add(Integer.parseInt((String)component));
            }
            i++;
        }
        return new ExactTag(builder.build());
    }

    
    private static boolean matchesMod2(long v, long mask, final int mod2) {
        while (mask != 0) {
            if (0 != (mask & 0xf)) {
                if ((v & 0xf) % 2 != mod2) {
                    return false;
                }
            }
            v >>= 4;
            mask >>= 4;
        }
        return true;
    }
    
    private static CharSequence safeSubseq(final CharSequence cs, final int begin, final int count) {
        if (null == cs || begin >= cs.length()) {
            return "";
        }
        final int end = Math.min(cs.length(), begin+count);
        return cs.subSequence(begin, end);
    }
        
    private static String reconstructSpec(final Iterable<String> spec) {
        final Iterator<String> i = spec.iterator();
        final StringBuilder sb = new StringBuilder(i.next());
        while (i.hasNext()) {
            sb.append('[').append(i.next()).append(']');
            sb.append(i.next());
        }
        return sb.toString();
    }

    private static List<String> normalizeComponents(final Iterable<String> components) throws ScriptEvaluationException {
        ImmutableList.Builder<String> builder = ImmutableList.builder();
        int i = 0;
        for (final String component : components) {
            if (0 == i%2) {
                final Matcher m = tagPattern.matcher(component);
                if (m.matches()) {
                    builder.add(m.group(1) + m.group(2));
                } else {
                    throw new ScriptEvaluationException("expected tag pattern (gggg,eeee); found " + component);
                }
            } else {
                final Matcher m = indexPattern.matcher(component);
                if (m.matches()) {
                    builder.add(m.group(1));
                } else {
                    throw new ScriptEvaluationException("expected sequence index [n]; found " + component);
                }
            }
            i++;
        }
        return builder.build();
    }
    
    private static List<String> tokenizeSpec(final CharSequence spec) throws ScriptEvaluationException {    
        int pos = 0;
        ImmutableList.Builder<String> builder = ImmutableList.builder();
        for (int i = 0; ; i++) {
            if (pos == spec.length()) {
                if (0 == i % 2) {
                    throw new ScriptEvaluationException("tag pattern must have odd component count");
                }
                return builder.build();
            }
            if (0 == i%2) {
                final Matcher m = tagPattern.matcher(safeSubseq(spec, pos, 11));
                if (m.matches()) {
                    builder.add(m.group(1) + m.group(2));
                    pos += 11;
                } else {
                    throw new ScriptEvaluationException("expected tag pattern (gggg,eeee); found " + safeSubseq(spec, pos, 9));                 
                }
            } else {
                final Matcher m = indexPattern.matcher(spec.subSequence(pos, spec.length()));
                if (m.lookingAt()) {
                    builder.add(m.group(1));    // add index without []
                    pos += m.group(0).length(); // skip past entire match
                } else {
                    throw new ScriptEvaluationException("expected sequence index [n]; found "
                            + spec.subSequence(pos, spec.length()) + " instead");
                }
            }
        }
    }
    
    /*
     * (non-Javadoc)
     * @see com.google.common.base.Function#apply(java.lang.Object)
     */
    public Iterable<int[]> apply(final DicomObject o) {
        final int[] exactPath = exactPath();
        if (null == exactPath) {
            return apply(Lists.<int[]>newArrayList(), o, Lists.<Integer>newArrayList());
        } else {
            return Collections.singleton(exactPath);
        }
    }

    private boolean apply(final int tag, final int offset) {
        final long t = tag & 0xffffffffL;
        return (t & exactMask[offset]) == exactPart[offset]
                && matchesMod2(t, evenMask[offset], 0)
                && matchesMod2(t, oddMask[offset], 1);
    }

    /*
     * (non-Javadoc)
     * @see com.google.common.base.Predicate#apply(java.lang.Object)
     */
    public boolean apply(final Integer tag) {
        return apply(tag, 0);
    }

    /**
     * Appends matching tagpaths to the tags list, then returns that list.
     * Assumes that this TagPattern is not exact -- those are filtered out in apply(DicomObject).
     * If the entire path matches up to the last element, and the final element is exact, matching
     * tagpaths are returned even if that final element does not appear in the template object.
     * @param tags accumulator for matching tagpaths
     * @param o DicomObject on which patterns are evaluated
     * @param path tagpath of o (empty for dataset root)
     * @return
     */
    private List<int[]> apply(final List<int[]> tags, final DicomObject o, final List<Integer> path) {
        final int depth = path.size();
        assert 0 == depth % 2 : "TagPattern.apply must have even path size";
        if (MASK_ALL == exactMask[depth]) {
            final int tag = (int)exactPart[depth];
            if (depth == exactMask.length - 1) {
                tags.add(tagpath(path, tag));	// yes, even if tag doesn't exist in template object
            } else {
                // descend only if there's a matching SQ
                final DicomElement e = o.get(tag);
                if (null != e && e.hasDicomObjects()) {
                    final List<Integer> childpath = Lists.newArrayList();	// really want persistent structure for this
                    childpath.add(tag);
                    childpath.add(-1);
                    for (int i = 0; i < e.countItems(); i++) {
                        if (anyMask[depth+1] != 0 || exactPart[depth+1] == i) {
                            childpath.set(depth+1, i);
                            apply(tags, e.getDicomObject(i), childpath);
                        }
                    }           		    				
                }
            }
        } else { // not exact so we need to scan
            for (final Iterator<DicomElement> ei = o.datasetIterator(); ei.hasNext(); ) {
                final DicomElement e = ei.next();
                final int tag = e.tag();
                if (apply(tag, depth)) {
                    if (depth == exactMask.length - 1) {
                        tags.add(tagpath(path, tag));
                    } else if (e.hasDicomObjects()) {
                        final List<Integer> childpath = Lists.newArrayList();	// really want persistent structure for this
                        childpath.add(tag);
                        for (int i = 0; i < e.countItems(); i++) {
                            if (anyMask[depth+1] != 0 || exactPart[depth+1] == i) {
                                childpath.set(depth+1, i);
                                apply(tags, e.getDicomObject(i), childpath);
                            }
                        }           		
                    } // else pattern expected this tag to be SQ but it's not. weird but probably ok.
                }
            }
        }
        return tags;
    }

    /**
     * If this pattern is exact, returns the concrete int[] tagpath; otherwise null.
     * @return concrete tagpath or null
     */
    private int[] exactPath() {
        for (final long m : exactMask) {
            if (m != MASK_ALL) {
                return null;
            }
        }
        final int[] tagpath = new int[exactPart.length];
        for (int i = 0; i < exactPart.length; i++) {
            tagpath[i] = (int)exactPart[i];
        }
        return tagpath;
    }

    /* (non-Javadoc)
     * @see org.nrg.dcm.edit.ElementMatcher#getTopTag()
     */
    public long getTopTag() {
        return exactPart[0] | (evenMask[0] & MAX_EVEN) | ((oddMask[0] | anyMask[0]) & MAX_ODD);
    }

    private int[] tagpath(final List<Integer> path, final int tag) {
        final int n = path.size();
        final int[] apath = new int[n+1];
        for (int i = 0; i < n; i++) {
            apath[i] = path.get(i);
        }
        apath[n] = tag;
        return apath;
    }

    /* TODO: hashCode equals */

    /*
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    public String toString() {
        return "TagPattern " + textpath;
    }
}
