/**
 * Copyright (c) 2009-2013 Washington University School of Medicine
 */
package org.nrg.dcm.edit;

import java.util.Map;

import org.dcm4che2.data.DicomObject;

/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public class Deletion extends AbstractOperation {
    private final ElementMatcher pattern;

    public Deletion(final ElementMatcher pattern) {
        super("Delete");
        this.pattern = pattern;
    }

    public Deletion(final int tag) {
        this(new ExactTag(tag));
    }

    /*
     * (non-Javadoc)
     * @see org.nrg.dcm.edit.Operation#affects(int)
     */
    public boolean affects(final int tag) {
        return pattern.apply(tag);
    }

    public Action makeAction(final DicomObject o) throws AttributeException {
        return new Action() {
            public void apply() {
                for (final int[] tag : pattern.apply(o)) {
                    o.remove(tag);
                }
            }

            public String toString() {
                return getName() + " " + pattern;
            }
        };
    }

    public String apply(Map<Integer,String> vals) { return null; }

    /*
     * (non-Javadoc)
     * @see org.nrg.dcm.edit.Operation#getTopTag()
     */
    public long getTopTag() { return pattern.getTopTag(); }

    /*
     * (non-Javadoc)
     * @see org.nrg.dcm.edit.Operation#replace(org.nrg.dcm.edit.Variable)
     */
    public void replace(final Variable _) {}

    /*
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    public String toString() {
        return "Delete " + pattern;
    }
}
