/**
 * Copyright (c) 2008-2011 Washington University
 */
package org.nrg.dcm.edit;

import java.util.SortedSet;

import org.dcm4che2.data.DicomObject;

/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
interface ConstraintMatch {
    SortedSet<Long> getTags();
    boolean matches(DicomObject o) throws ScriptEvaluationException;
}
