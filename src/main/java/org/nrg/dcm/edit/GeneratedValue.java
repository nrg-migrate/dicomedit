/**
 * Copyright (c) 2009-2013 Washington University School of Medicine
 */
package org.nrg.dcm.edit;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;

import org.dcm4che2.data.BasicDicomObject;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.VR;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public class GeneratedValue implements Value {
    private static final DicomObject templo = new BasicDicomObject();
    private final GeneratorScope scope;
    private final int tag;
    private final String label;
    private final List<Value> params;
    private final VR vr;

    public GeneratedValue(final GeneratorScope scope, final int tag, final String label, final List<Value> params) {
        this.scope = scope;
        this.tag = tag;
        this.label = label;
        this.params = ImmutableList.copyOf(params);
        this.vr = templo.vrOf(tag);
    }


    /* (non-Javadoc)
     * @see org.nrg.dcm.edit.Value#getTags()
     */
    public SortedSet<Long> getTags() {
        final SortedSet<Long> tags = Sets.newTreeSet();
        for (final Value param : params) {
            tags.addAll(param.getTags());
        }
        return tags;
    }

    /* (non-Javadoc)
     * @see org.nrg.dcm.edit.Value#getVariables()
     */
    public Set<Variable> getVariables() {
        final Set<Variable> vars = Sets.newLinkedHashSet();
        for (final Value v : params) {
            vars.addAll(v.getVariables());
        }
        return vars;
    }

    /* (non-Javadoc)
     * @see org.nrg.dcm.edit.Value#on(org.dcm4che2.data.DicomObject)
     */
    public String on(final DicomObject o) throws ScriptEvaluationException {
        final List<String> values = Lists.newArrayListWithExpectedSize(params.size());
        for (final Value v : params) {
            values.add(v.on(o));
        }
        return scope.getValue(label, tag, o.getString(tag), values);
    }

    /* (non-Javadoc)
     * @see org.nrg.dcm.edit.Value#on(java.util.Map)
     */
    public String on(final Map<Integer,String> m) throws ScriptEvaluationException {
        final List<String> values = Lists.newArrayListWithExpectedSize(params.size());
        for (final Value v : params) {
            values.add(v.on(m));
        }
        return scope.getValue(label, tag, m.get(tag), values);
    }
    
    /*
     * (non-Javadoc)
     * @see org.nrg.dcm.edit.Value#replaceVariable(org.nrg.dcm.edit.Variable)
     */
    public void replace(final Variable var) {
        for (final Value val : params) {
            val.replace(var);
        }
    }

    /* (non-Javadoc)
     * @see org.nrg.dcm.edit.Value#vr()
     */
    public VR vr() { return vr; }

    /* (non-Javadoc)
     * @see org.nrg.dcm.edit.Value#vrOn(org.dcm4che2.data.DicomObject)
     */
    public VR vrOn(final DicomObject o) {
        return o.vrOf(tag);
    }


    /* (non-Javadoc)
     * @see org.nrg.dcm.edit.Value#bytesOn(org.dcm4che2.data.DicomObject)
     */
    public byte[] bytesOn(DicomObject o) throws ScriptEvaluationException {
        final String s = on(o);
        return null == s ? null : s.getBytes();
    }
}
