/**
 * Copyright (c) 2006-2013 Washington University
 */
package org.nrg.dcm.edit;

import java.io.File;
import java.util.Collections;
import java.util.Set;
import java.util.SortedSet;

import org.dcm4che2.data.DicomObject;

import com.google.common.collect.Sets;

/**
 * Represents a constraint on an Operation.
 * @author Kevin A. Archie <karchie@wustl.edu>
 */
public class Constraint {
    private static final Iterable<File> empty = Collections.emptyList();
    private final ConstraintMatch m;
    private final Set<File> files;

    public Constraint(final ConstraintMatch m) {
        this(m, empty);
    }

    public Constraint(final ConstraintMatch m, final File f) {
        this(m, Collections.singletonList(f));
    }

    public Constraint(final ConstraintMatch m, final Iterable<File> fs) {
        this.m = m;
        files = Sets.newHashSet(fs);
        if (null == m && files.isEmpty()) {
            throw new IllegalArgumentException("Either value constraint or file list must be provided");
        }
    }

    boolean matches(final File f, final DicomObject o) throws ScriptEvaluationException {
        assert m != null || !files.isEmpty();
        if (m != null && !m.matches(o)) {
            return false;
        }
        return files.isEmpty() || files.contains(f);
    }

    public SortedSet<Long> getTags() {
        return m.getTags();
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    public String toString() { return m.toString(); }
}
