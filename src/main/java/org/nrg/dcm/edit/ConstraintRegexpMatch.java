/*
 * Copyright (c) 2006-2009 Washington University
 */
package org.nrg.dcm.edit;

/**
 * @author Kevin A. Archie <karchie@npg.wustl.edu>
 *
 */
class ConstraintRegexpMatch extends SimpleConstraintMatch {
    ConstraintRegexpMatch(final int tag, final String pattern) {
	super(tag, pattern);
    }
    
    ConstraintRegexpMatch(final Integer tag, final String pattern) {
	this(tag.intValue(), pattern);
    }

    /*
     * (non-Javadoc)
     * @see org.nrg.dcm.edit.SimpleConstraintMatch#matches(java.lang.String)
     */
    public boolean matches(final String value) {
	return value.matches(getPattern());
    }
}
