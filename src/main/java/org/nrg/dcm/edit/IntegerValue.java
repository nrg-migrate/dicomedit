/**
 * Copyright (c) 2010-2013 Washington University School of Medicine
 */
package org.nrg.dcm.edit;

import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;

import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.VR;

import com.google.common.collect.ImmutableSortedSet;

/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public final class IntegerValue implements Value {
    private final String sval;
    private final int val;

    public IntegerValue(final String value) {
        sval = value;
        val = Integer.parseInt(value);
    }
    
    public IntegerValue(final int value) {
        sval = String.valueOf(value);
        val = value;
    }

    /* (non-Javadoc)
     * @see org.nrg.dcm.edit.Value#getTags()
     */
    public SortedSet<Long> getTags() { return ImmutableSortedSet.of(); }

    /* (non-Javadoc)
     * @see org.nrg.dcm.edit.Value#getVariables()
     */
    public Set<Variable> getVariables() { return Collections.emptySet(); }

    /* (non-Javadoc)
     * @see org.nrg.dcm.edit.Value#on(org.dcm4che2.data.DicomObject)
     */
    public String on(final DicomObject o) {
        return sval;
    }

    /* (non-Javadoc)
     * @see org.nrg.dcm.edit.Value#on(java.util.Map)
     */
    public String on(final Map<Integer,String> m) {
        return sval;
    }

    public int getValue() { return val; }
    
    /*
     * (non-Javadoc)
     * @see org.nrg.dcm.edit.Value#replaceVariable(org.nrg.dcm.edit.Variable)
     */
    public void replace(final Variable _) {}

    /* (non-Javadoc)
     * @see org.nrg.dcm.edit.Value#vr()
     */
    public VR vr() { return VR.IS; }

    /* (non-Javadoc)
     * @see org.nrg.dcm.edit.Value#vrOn(org.dcm4che2.data.DicomObject)
     */
    public VR vrOn(final DicomObject _) { return VR.IS; }

    /* (non-Javadoc)
     * @see org.nrg.dcm.edit.Value#bytesOn(org.dcm4che2.data.DicomObject)
     */
    public byte[] bytesOn(final DicomObject o) throws ScriptEvaluationException {
        final String s = on(o);
        return null == s ? null : s.getBytes();
    }
}
