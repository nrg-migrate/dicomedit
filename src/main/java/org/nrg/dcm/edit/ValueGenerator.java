/**
 * Copyright (c) 2009-2011 Washington University
 */
package org.nrg.dcm.edit;

/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 */
public interface ValueGenerator {
    /**
     * Returns a new value to replace the provided previous value.
     * @param old previous value
     * @param values concrete (String) values of generator parameters
     * @return generated value
     * @throws ScriptEvaluationException
     */
    String valueFor(String old, Iterable<String> values) throws ScriptEvaluationException;
}