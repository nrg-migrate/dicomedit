/**
 * Copyright (c) 2010,2011 Washington University
 */
package org.nrg.dcm.edit;

import java.util.List;

/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public interface ScriptFunction {
    Value apply(List<? extends Value> args) throws ScriptEvaluationException;
}
