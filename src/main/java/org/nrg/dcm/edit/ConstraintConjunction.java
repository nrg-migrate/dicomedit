/**
 * Copyright (c) 2008-2011 Washington University
 */
package org.nrg.dcm.edit;

import java.util.Collections;
import java.util.SortedSet;

import org.dcm4che2.data.DicomObject;

import com.google.common.base.Joiner;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public class ConstraintConjunction implements ConstraintMatch {
    private final Iterable<ConstraintMatch> predicates;

    public ConstraintConjunction(final ConstraintMatch predicate) {
        this(Collections.singletonList(predicate));
    }

    public ConstraintConjunction(final Iterable<ConstraintMatch> predicates) {
        this.predicates = Lists.newArrayList(predicates);
    }

    public SortedSet<Long> getTags() {
        final SortedSet<Long> tags = Sets.newTreeSet();
        for (final ConstraintMatch p : predicates) {
            tags.addAll(p.getTags());   
        }
        return tags;
    }

    /* (non-Javadoc)
     * @see org.nrg.dcm.edit.ConstraintMatch#matches(org.dcm4che2.data.DicomObject)
     */
    public boolean matches(final DicomObject o) throws ScriptEvaluationException {
        for (final ConstraintMatch predicate : predicates) {
            if (!predicate.matches(o)) {
                return false;
            }
        }
        return true;
    }

    public String toString() {
        final StringBuilder sb = new StringBuilder("ConstraintConjunction: ");
        if (Iterables.isEmpty(predicates)) {
            sb.append("[TRUE]");
        } else {
            Joiner.on(" AND ").appendTo(sb, predicates);
        }
        return sb.toString();
    }

}
