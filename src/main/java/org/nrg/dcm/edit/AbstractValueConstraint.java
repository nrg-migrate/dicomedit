/**
 * Copyright (c) 2006-2013 Washington University School of Medicine
 */
package org.nrg.dcm.edit;

import java.util.SortedSet;

import org.dcm4che2.data.DicomObject;

import com.google.common.collect.ImmutableSortedSet;

/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
abstract class AbstractValueConstraint implements ConstraintMatch {
    private final Value left, right;

    AbstractValueConstraint(final Value left, final Value right) {
        this.left = left;
        this.right = right;
    }

    abstract protected boolean matches(String pattern, String value) throws ScriptEvaluationException;

    public SortedSet<Long> getTags() {
        final ImmutableSortedSet.Builder<Long> tags = ImmutableSortedSet.naturalOrder();
        tags.addAll(left.getTags());
        tags.addAll(right.getTags());
        return tags.build();
    }

    final public boolean matches(final DicomObject o) throws ScriptEvaluationException {
        return matches(left.on(o), right.on(o));
    }

    public String toString(final String operation) {
        return "Constraint: " + left + " " + operation + " " + right;
    }
}
