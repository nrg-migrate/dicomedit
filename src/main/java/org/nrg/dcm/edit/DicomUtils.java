/**
 * Copyright (c) 2009-2011 Washington University
 */
package org.nrg.dcm.edit;

import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import java.util.TimeZone;
import java.util.regex.Pattern;

import org.dcm4che2.data.DicomElement;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.Tag;
import org.dcm4che2.data.UID;
import org.dcm4che2.data.VR;

import com.google.common.base.Joiner;
import com.google.common.collect.ImmutableMap;


/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public final class DicomUtils {
    private DicomUtils() {}	// prevent instantiation

    private static StringBuffer join(final StringBuffer sb, final int[] array, final String separator) {
        if (array.length > 0) {
            sb.append(array[0]);
            for (int i = 1; i < array.length; i++) {
                sb.append(separator);
                sb.append(array[i]);
            }
        }
        return sb;
    }

    private static String join(final int[] array, final String separator) {
        return join(new StringBuffer(), array, separator).toString();
    }

    private static interface Converter {
        String convert(DicomObject o, DicomElement e) throws AttributeVRMismatchException;
    }

    private final static Map<VR,Converter> conversions = 
        ImmutableMap.of(VR.SQ, new Converter() {
            public String convert(final DicomObject o, final DicomElement e)
            throws AttributeVRMismatchException {
                throw new AttributeVRMismatchException(e.tag(), e.vr());
            }
        },
        VR.UN, new Converter() {
            public String convert(final DicomObject o, final DicomElement e) {
                return e.getString(o.getSpecificCharacterSet(), false);
            }
        },
        VR.AT, new Converter() {
            public String convert(final DicomObject o, final DicomElement e) {
                return join(e.getInts(false), "\\");
            }
        },
        VR.OB, new Converter() {
            public String convert(final DicomObject o, final DicomElement e) {
                return VR.OB.toString(e.getBytes(), o.bigEndian(), o.getSpecificCharacterSet());
            }
        });


    public static String getString(final DicomObject o, final int...tagpath)
    throws AttributeVRMismatchException {
        final DicomElement de = o.get(tagpath);
        if (null == de) {
            return null;
        }
        final Converter converter = (Converter)conversions.get(de.vr());
        if (null == converter) {
            // all other data types can be treated as simple strings, maybe with
            // multiple values separated by backslashes.  Join these.
            try {
                return Joiner.on("\\").join(de.getStrings(o.getSpecificCharacterSet(), false));
            } catch (UnsupportedOperationException e) {
                throw new AttributeVRMismatchException(tagpath[tagpath.length-1], de.vr());
            }
        } else {
            return converter.convert(o, de);
        }
    }


    private final static Pattern VALID_UID_PATTERN = Pattern.compile("(0|([1-9][0-9]*))(\\.(0|([1-9][0-9]*)))*");
    private final static int UID_MIN_LEN = 1;
    private final static int UID_MAX_LEN = 64;

    public static boolean isValidUID(final CharSequence uid) {
        if (null == uid) {
            return false;
        }
        final int len = uid.length();
        if (len < UID_MIN_LEN || UID_MAX_LEN < len) {
            return false;
        }
        return VALID_UID_PATTERN.matcher(uid).matches();
    }


    /**
     * Returns the Transfer Syntax UID of the given DICOM object.
     * @param o DICOM object
     * @return Transfer Syntax UID
     */
    public static final String getTransferSyntaxUID(final DicomObject o) {
        // Default TS is Implicit VR LE (PS 3.5, Section 10)
        return o.getString(Tag.TransferSyntaxUID, UID.ImplicitVRLittleEndian);
    }


    private static final TimeZone TIME_ZONE = Calendar.getInstance().getTimeZone();

    /**
     * Returns a new Date object that represents a date/time combination from the named
     * DA and TM attributes of the given DicomObject.  Assumes, but does not verify,
     * that attributes are of VR DA and TM, respectively.
     * @param o DicomObject from which date/time should be extracted
     * @param dateTag DA attribute
     * @param timeTag TM attribute
     * @return combined Date object
     */
    public static Date getDateTime(final DicomObject o, final int dateTag, final int timeTag) {
        final Date date = o.getDate(dateTag);
        final Date time = o.getDate(timeTag);
        if (null == date) {
            return time;
        } else if (null == time) {
            return date;
        } else {
        	if(TIME_ZONE.inDaylightTime(date)) {
        		Calendar localTime = Calendar.getInstance(TIME_ZONE);
        		localTime.setTime(date);
        		
        		Calendar fixTime = Calendar.getInstance();
        		fixTime.setTime(time);
        		localTime.set(Calendar.HOUR_OF_DAY, fixTime.get(Calendar.HOUR_OF_DAY));
        		localTime.set(Calendar.MINUTE, fixTime.get(Calendar.MINUTE));
        		return new Date(localTime.getTimeInMillis());
        	} else {
        		return new Date(date.getTime() + time.getTime() + TIME_ZONE.getOffset(date.getTime()));
        	}
        }
    }
}
