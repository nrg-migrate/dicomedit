/*
 * Copyright (c) 2006-2013 Washington University
 */

package org.nrg.dcm.edit;

import java.util.Map;

import org.dcm4che2.data.DicomObject;

/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public interface Operation {
    /**
     * Creates an Action that is a concrete implementation of applying this
     * Operation to the given DicomObject.
     * @param o DicomObject to act on
     * @return
     * @throws AttributeException
     */
    Action makeAction(DicomObject o) throws AttributeException,ScriptEvaluationException;

    /**
     * Compute the value that results from applying this Operation to
     * the given object context.
     * @param vals Map:Tag(Integer) -> Value(String) representing the object of the operation
     * @return String value
     * @throws ScriptEvaluationException
     */
    String apply(Map<Integer,String> vals) throws ScriptEvaluationException;

    boolean affects(int tag);
    
    /**
     * Maximum tag value potentially required for or modified by this operation.
     * @return
     */
    long getTopTag();

    /**
     * Returns the name of this operation
     * @return String name of this operation
     */
    String getName();
    
    /**
     * Uses the provided Variable v to replace any similarly-named Variables
     * inside this Operation; this is for value unification across scripts.
     * @param v
     */
    void replace(Variable v);
}
