/**
 * Copyright (c) 2009-2014 Washington University School of Medicine
 */
package org.nrg.dcm.edit;

import java.util.Arrays;
import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;

import org.dcm4che2.data.BasicDicomObject;
import org.dcm4che2.data.DicomElement;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.VR;
import org.dcm4che2.util.TagUtils;

import com.google.common.collect.ImmutableSortedSet;

/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public final class SingleTagValue implements Value {
    private static final DicomObject templo = new BasicDicomObject();
    private final int[] tagpath;
    private final VR vr;

    public SingleTagValue(final VR vr, final int...tagpath) {
        if (0 == tagpath.length % 2) {
            throw new IllegalArgumentException("tagpath must have odd number of components");
        }
        this.vr = vr;
        this.tagpath = Arrays.copyOf(tagpath, tagpath.length);
    }

    public SingleTagValue(final int...tagpath) {
        this(templo.vrOf(tag(tagpath)), tagpath);
    }

    private static int tag(final int[] tagpath) {
        return tagpath[tagpath.length-1];
    }

    /*
     * (non-Javadoc)
     * @see org.nrg.dcm.edit.Value#getTags()
     */
    public SortedSet<Long> getTags() {
        return ImmutableSortedSet.of((long)tagpath[0]);
    }

    /*
     * (non-Javadoc)
     * @see org.nrg.dcm.edit.Value#getVariables()
     */
    public Set<Variable> getVariables() {
        return Collections.emptySet();
    }

    /* (non-Javadoc)
     * @see org.nrg.dcm.edit.Value#on(org.dcm4che2.data.DicomObject)
     */
    public String on(final DicomObject o)
            throws ScriptEvaluationException {
        try {
            return DicomUtils.getString(o, tagpath);
        } catch (AttributeVRMismatchException e) {
            throw new ScriptEvaluationException(e);
        }
    }

    /*
     * (non-Javadoc)
     * @see org.nrg.dcm.edit.Value#on(java.util.Map)
     */
    public String on(final Map<Integer,String> m) {
        if (1 == tagpath.length) {
            return m.get(tagpath[0]);
        } else {
            return null;
        }
    }

    /*
     * (non-Javadoc)
     * @see org.nrg.dcm.edit.Value#replaceVariable(org.nrg.dcm.edit.Variable)
     */
    public void replace(final Variable _) {}

    /*
     * (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    public boolean equals(final Object o) {
        return o instanceof SingleTagValue && tagpath.equals(((SingleTagValue)o).tagpath);
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    public int hashCode() {
        return Arrays.hashCode(tagpath);
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    public String toString() {
        final StringBuilder sb = new StringBuilder("value-of");
        for (int i = 0; i < tagpath.length; i+=2) {
            sb.append(TagUtils.toString(tagpath[i]));
            if (i+1 < tagpath.length) {
                sb.append(tagpath[i+1]);
            }
        }
        return sb.toString();
    }

    /* (non-Javadoc)
     * @see org.nrg.dcm.edit.Value#vr()
     */
    public VR vr() { return vr; }

    /* (non-Javadoc)
     * @see org.nrg.dcm.edit.Value#vrOn(org.dcm4che2.data.DicomObject)
     */
    public VR vrOn(final DicomObject o) {
        final VR dictvr = o.vrOf(tag(tagpath));
        if (VR.UN.equals(dictvr)) {
            final DicomElement e = o.get(tagpath);
            return null == e ? VR.UN : e.vr();
        } else {
            return dictvr;
        }
    }

    /* (non-Javadoc)
     * @see org.nrg.dcm.edit.Value#bytesOn(org.dcm4che2.data.DicomObject)
     */
    public byte[] bytesOn(final DicomObject o) {
        return o.getBytes(tagpath);
    }
}
