/**
 * Copyright (c) 2014 Washington University School of Medicine
 */
package org.nrg.dcm.edit;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.dcm4che2.data.DicomElement;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.util.TagUtils;

import com.google.common.collect.Lists;

/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public class NestableTag implements ElementMatcher {
    private static final long MAX_TAG = Integer.MAX_VALUE;
    private final int tag;
    
    public NestableTag(final int tag) {
        this.tag = tag;
    }
    
    /**
     * Descend through all nested tags looking for matches
     * @param tagpaths matched tag paths
     * @param o DicomObject to examine
     * @param path tagpath of o within root object (empty for root object)
     * @return tagpaths
     */
    private List<int[]> walk(final List<int[]> tagpaths, final DicomObject o, final List<Integer> path) {
        for (Iterator<DicomElement> ei = o.datasetIterator(); ei.hasNext();) {
            final DicomElement e = ei.next();
            final int depth = path.size();
            if (e.tag() == tag) {
                final int[] a = new int[depth + 1];
                for (int i = 0; i < depth; i++) {
                    a[i] = path.get(i);
                }
                a[depth] = tag;
                tagpaths.add(a);
            }
            if (e.hasDicomObjects()) {
                final List<Integer> nextpath = Lists.newArrayList(path);
                nextpath.add(e.tag());
                nextpath.add(-1);
                for (int i = 0; i < e.countItems(); i++) {
                    nextpath.set(depth+1, i);
                    walk(tagpaths, e.getDicomObject(i), nextpath);
                }
            }
        }
        return tagpaths;
    }
    
    /* (non-Javadoc)
     * @see com.google.common.base.Function#apply(java.lang.Object)
     */
    public Iterable<int[]> apply(final DicomObject o) {
        return walk(Lists.<int[]>newArrayList(), o, Collections.<Integer>emptyList());
    }

    /* (non-Javadoc)
     * @see com.google.common.base.Predicate#apply(java.lang.Object)
     */
    public boolean apply(final Integer tag) {
        return null != tag && this.tag == tag;
    }

    /* (non-Javadoc)
     * @see org.nrg.dcm.edit.ElementMatcher#getTopTag()
     */
    public long getTopTag() {
        return MAX_TAG;
    }
    
    /*
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    public String toString() {
        return "*" + TagUtils.toString(tag);
    }
}
