/**
 * Copyright (c) 2009 Washington University
 */
package org.nrg.io;

import java.io.File;

@Deprecated
public interface FileMapper {
    File apply(File original);
}
