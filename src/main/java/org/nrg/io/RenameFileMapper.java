/**
 * Copyright (c) 2009,2011 Washington University
 */
package org.nrg.io;

import java.io.File;
import java.text.MessageFormat;

import com.google.common.base.Function;

/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public final class RenameFileMapper implements Function<File,File> {
    private final String format;

    public RenameFileMapper(final String format) {
        this.format = format;
    }

    /* (non-Javadoc)
     * @see org.nrg.dcm.edit.FileMapper#map(java.io.File)
     */
    public File apply(final File original) {
        final File dir = original.getParentFile();
        final String name = MessageFormat.format(format,
                new Object[]{original.getName()});
        return new File(dir, name);
    }
}
