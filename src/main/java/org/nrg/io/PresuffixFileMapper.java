/**
 * Copyright (c) 2009,2011 Washington University
 */
package org.nrg.io;

import java.io.File;

import com.google.common.base.Function;

/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public final class PresuffixFileMapper implements Function<File,File> {
    private final String presuffix;

    public PresuffixFileMapper(final String presuffix) {
        this.presuffix = presuffix;
    }


    /* (non-Javadoc)
     * @see org.nrg.dcm.edit.FileMapper#map(java.io.File)
     */
    public File apply(final File original) {
        final File dir = original.getParentFile();
        final StringBuilder name = new StringBuilder(original.getName());
        final int presuffixLoc = name.lastIndexOf(".");
        if (presuffixLoc < 0) {
            name.append(presuffix);
        } else {
            name.insert(presuffixLoc, presuffix);
        }
        return new File(dir, name.toString());
    }
}
