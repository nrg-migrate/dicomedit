package org.nrg.util;

import java.io.IOException;
import java.net.URL;

public interface IHttpClient {
	String request(URL url) throws IOException;
}
