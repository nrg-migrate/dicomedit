/**
 * Copyright (c) 2012 Washington University
 */
package org.nrg.util;

import java.io.File;

/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public final class NamedFileFunction<V,E extends Exception> implements
        CheckedExceptionFunction<String,V, E> {
    private final CheckedExceptionFunction<File,V,E> f;
    
    public NamedFileFunction(final CheckedExceptionFunction<File,V,E> f) {
        this.f = f;
    }
    
    public V apply(final String name) throws E {
        return f.apply(new File(name));
    }
}
