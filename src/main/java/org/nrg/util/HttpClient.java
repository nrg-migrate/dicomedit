package org.nrg.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class HttpClient implements IHttpClient {
    private static final String AUTHORIZATION_HEADER = "Authorization";

    public String request(final URL url) throws IOException {
        final HttpURLConnection connection;
        try {
            connection = (HttpURLConnection) url.openConnection();
        } catch (ClassCastException e) {
            throw new IOException("unable to make HTTP/HTTPS connection to URL " + url);
        }

        if (url.getUserInfo() != null) {
            final StringBuilder authHeader = new StringBuilder("Basic ");
            authHeader.append(Base64.encode(url.getUserInfo()));
            connection.setRequestProperty(AUTHORIZATION_HEADER, authHeader.toString());
        }

        final StringBuilder text = new StringBuilder();
        connection.connect();
        try {
            if (HttpURLConnection.HTTP_OK != connection.getResponseCode()) {
                throw new IOException("request failed: " + connection.getResponseCode() + " " + connection.getResponseMessage());
            }

            final BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            for (String line = reader.readLine(); null != line; line = reader.readLine()) {
                text.append(line);
            }
            reader.close();
        } finally {
            connection.disconnect();
        }
        return text.toString();
    }
}
