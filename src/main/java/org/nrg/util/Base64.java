package org.nrg.util;


public class Base64 {
	/**
	 * Base64 encoding adapted from
	 * http://www.wikihow.com/Encode-a-String-to-Base64-With-Java Article
	 * authors John Comeau, et al.
	 */
	private static final String base64code = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

	private static final int splitLinesAt = 76;

	public static String encode(final String string) {
		final StringBuilder encoded = new StringBuilder();
		byte[] stringArray;
		try {
			stringArray = string.getBytes("UTF-8"); // use appropriate encoding
													// string
		} catch (Exception ignored) {
			stringArray = string.getBytes(); // use locale default rather than
												// croak
		}

		// determine how many padding bytes to add to the output
		int paddingCount = (3 - (stringArray.length % 3)) % 3;

		// add any necessary padding to the input
		stringArray = zeroPad(stringArray.length + paddingCount, stringArray);

		// process 3 bytes at a time, churning out 4 output bytes
		// worry about CRLF insertions later
		for (int i = 0; i < stringArray.length; i += 3) {
			final int j = (stringArray[i] << 16) + (stringArray[i + 1] << 8) + stringArray[i + 2];
			encoded.append(base64code.charAt((j >> 18) & 0x3f));
			encoded.append(base64code.charAt((j >> 12) & 0x3f));
			encoded.append(base64code.charAt((j >> 6) & 0x3f));
			encoded.append(base64code.charAt(j & 0x3f));
		}

		// replace encoded padding nulls with "="
		return splitLines(encoded.substring(0, encoded.length() - paddingCount) + "==".substring(0, paddingCount));
	}

	public static String splitLines(final String string) {
		final StringBuilder lines = new StringBuilder();
		lines.append(string.substring(0, Math.min(string.length(), splitLinesAt)));
		for (int i = splitLinesAt; i < string.length(); i += splitLinesAt) {
			lines.append("\r\n");
			lines.append(string.substring(i, Math.min(string.length(), i + splitLinesAt)));
		}
		return lines.toString();
	}

	public static byte[] zeroPad(final int length, final byte[] bytes) {
		final byte[] padded = new byte[length];
		System.arraycopy(bytes, 0, padded, 0, bytes.length);
		return padded;
	}

}
