/**
 * Copyright (c) 2012 Washington University
 */
package org.nrg.util;

/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public interface CheckedExceptionFunction<K,V,E extends Exception> {
    V apply(K k) throws E;
}
