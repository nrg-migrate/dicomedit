/**
 * Copyright (c) 2009 Washington University
 */
package org.nrg.util;

/**
 * Interface for progress notification (e.g., for progress bars)
 * This is partially compatible with the Swing ProgressMonitor,
 * though it leaves out a few methods.
 * @author Kevin A. Archie <karchie@npg.wustl.edu>
 */
public interface EditProgressMonitor {
  void setMinimum(int min);
  void setMaximum(int max);
  void setProgress(int current);
  void setNote(String note);
  void close();
  boolean isCanceled();
}
