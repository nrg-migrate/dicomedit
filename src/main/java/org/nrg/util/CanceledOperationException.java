/**
 * Copyright (c) 2009,2011 Washington University
 */
package org.nrg.util;

import com.google.common.base.Objects;

/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public final class CanceledOperationException extends Exception {
    private static final long serialVersionUID = 1L;

    /**
     * 
     */
    public CanceledOperationException() {
        this("operation canceled by user");
    }

    /**
     * @param message
     */
    public CanceledOperationException(final String message) {
        super(message);
    }

    /**
     * @param cause
     */
    public CanceledOperationException(final Throwable cause) {
        super(cause);
    }

    /**
     * @param message
     * @param cause
     */
    public CanceledOperationException(final String message, final Throwable cause) {
        super(message, cause);
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    public int hashCode() {
        return Objects.hashCode(getMessage(), getCause());
    }


    /*
     * (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    public boolean equals(final Object o) {
        if (o instanceof CanceledOperationException) {
            final Exception e = (Exception)o;
            return Objects.equal(getMessage(), e.getMessage()) &&
            Objects.equal(getCause(), e.getCause());
        } else {
            return false;
        }
    }
}